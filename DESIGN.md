# Gitlab-GLib Design

This API is very rudimentary so far and almost certainly doesn't do anything you need.

## Secrets

To use the Gitlab API, you need an API key or other form of credentials.
Currently, only an API key is supported.
You may generate an API key from your user account in the Gitlab web interface.

In the future, I hope to support interactive authorization using WebKit.

For now, use `GitlabApiKey`.

## Client

The `GitlabClient` provides access to high-level services of Gitlab.
Create a `GitlabClient` using your `GitlabApiKey` to establish a communication client to use Gitlab services.

## Services

Various services are broken into their own `GitlabService` subclass.
For example, you can access projects using `GitlabProjects` service.
You can access issues using `GitlabIssues` service.

## List Models

Since the primary way to use GTK data-binding is through `GListModel` the Gitlab-GLib library attempts to bridge pagination and list models.

The object within those list models is a `GitlabListItem` which can lazily load the underlying resource.
This can be useful because it allows scrollability even while data is loading from the client.

### Filtering

List models can be filtered by providing the appropriate filter type to the list creation method.
Different services have different filter types such as `GitlabIssueFilter`.

All filters are derived from `GitlabFilter`.

## Resources

The `GitlabResource` base class represents a view of a resource on the Gitlab server.
It is read-only and in fact the data behind it is represented from a `JsonNode` of a `JsonObject`.

## Creating Resources

Creating resources is serpated from viewing data objects as those are really just immutable content we got from the server.

Currently this is not implemented but the expection is that we have a `GitlabResourceBuilder` which can create the necessary parameters for resource creation.
