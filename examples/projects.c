#include <unistd.h>

#include <gitlab-glib.h>

static gboolean done;

static DexFuture *
list_all_projects (DexFuture *completed,
                 gpointer   user_data)
{
  g_autoptr(GListModel) model = dex_await_object (dex_ref (completed), NULL);
  guint n_items = g_list_model_get_n_items (model);

  g_print ("%d projects\n", n_items);

  for (guint i = 0; i < n_items; i++)
    {
      g_autoptr(GitlabListItem) list_item = g_list_model_get_item (model, i);
      GitlabProject *project = gitlab_list_item_get_item (list_item);

      if (project == NULL)
        continue;

      g_print ("     ID: %"G_GINT64_FORMAT"\n", gitlab_project_get_id (project));
      g_print ("   Name: %s\n", gitlab_project_get_name (project));
      g_print ("\n");
      g_print ("%s\n", gitlab_project_get_description (project));
      g_print ("===\n");
    }

  return NULL;
}

static DexFuture *
projects (const char *host,
          const char *api_key)
{
  g_autoptr(GitlabApiKey) secret = gitlab_api_key_new (api_key);
  g_autoptr(GitlabClient) client = gitlab_client_new (host, GITLAB_SECRET (secret));
  g_autoptr(GitlabProjects) projects = gitlab_projects_new (client);
  g_autoptr(GitlabProjectFilter) filter = gitlab_project_filter_new ();
  g_autoptr(GitlabListModel) list = NULL;

  gitlab_project_filter_set_owned (filter, TRUE);

  list = gitlab_projects_list (projects, filter);

  return dex_future_then (gitlab_list_model_populate_all (list),
                          list_all_projects, NULL, NULL);
}

static DexFuture *
exit_main_loop (DexFuture *completed,
                gpointer   user_data)
{
  g_autoptr(GError) error = NULL;

  if (!dex_await (dex_ref (completed), &error))
    g_printerr ("ERROR: %s\n", error->message);

  g_main_loop_quit (user_data);
  done = TRUE;
  return NULL;
}

int
main (int argc,
      char *argv[])
{
  g_autoptr(GOptionContext) context = g_option_context_new ("- gitlab projects tool");
  g_autoptr(GMainLoop) main_loop = g_main_loop_new (NULL, FALSE);
  g_autoptr(GError) error = NULL;
  g_autofree char *api_key = NULL;
  g_autofree char *host = NULL;
  GOptionEntry entries[] = {
    { "host", 'h', 0, G_OPTION_ARG_STRING, &host },
    { "api-key", 'k', 0, G_OPTION_ARG_STRING, &api_key },
    { 0 }
  };

  g_option_context_add_main_entries (context, entries, NULL);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("Error: %s\n", error->message);
      return EXIT_FAILURE;
    }

  if (host == NULL)
    {
      g_printerr ("Please provide a host name! (Example: gitlab.gnome.org)\n");
      return EXIT_FAILURE;
    }

  if (api_key == NULL)
    {
      g_printerr ("Please provide an API key!\n");
      return EXIT_FAILURE;
    }

  dex_future_disown (dex_future_finally (projects (host, g_strstrip (api_key)),
                                         exit_main_loop,
                                         main_loop,
                                         NULL));

  if (!done)
    g_main_loop_run (main_loop);

  return 0;
}
