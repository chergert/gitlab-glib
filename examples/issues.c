#include <unistd.h>

#include <gitlab-glib.h>

static gboolean done;

static char *
get_date_time (GitlabIssue *issue,
               const char  *property)
{
  g_autoptr(GDateTime) dt = NULL;

  g_object_get (issue, property, &dt, NULL);

  if (dt == NULL)
    return g_strdup ("-- Unset --");

  return g_date_time_format (dt, "%x %X");
}

static DexFuture *
list_all_issues (DexFuture *completed,
                 gpointer   user_data)
{
  g_autoptr(GListModel) model = dex_await_object (dex_ref (completed), NULL);
  guint n_items = g_list_model_get_n_items (model);

  for (guint i = 0; i < n_items; i++)
    {
      g_autoptr(GitlabListItem) list_item = g_list_model_get_item (model, i);
      GitlabIssue *issue = gitlab_list_item_get_item (list_item);
      g_autofree char *created_str = NULL;
      g_autofree char *updated_str = NULL;
      g_autofree char *closed_str = NULL;

      if (issue == NULL)
        continue;

      created_str = get_date_time (issue, "created-at");
      updated_str = get_date_time (issue, "updated-at");
      closed_str = get_date_time (issue, "closed-at");

      g_print ("     ID: %"G_GINT64_FORMAT"\n", gitlab_issue_get_id (issue));
      g_print ("  Title: %s\n", gitlab_issue_get_title (issue));
      g_print ("  State: %s\n", gitlab_issue_get_state (issue));
      g_print ("Created: %s\n", created_str);
      g_print ("Updated: %s\n", updated_str);
      g_print (" Closed: %s\n", closed_str);
      g_print ("\n");
      g_print ("%s\n", gitlab_issue_get_description (issue));
      g_print ("===\n");
    }

  return NULL;
}

static DexFuture *
issues (const char *host,
        const char *api_key,
        int         project_id,
        const char *project_name)
{
  g_autoptr(GitlabApiKey) secret = gitlab_api_key_new (api_key);
  g_autoptr(GitlabClient) client = gitlab_client_new (host, GITLAB_SECRET (secret));
  g_autoptr(GitlabIssues) issues = gitlab_issues_new (client);
  g_autoptr(GitlabListModel) list = NULL;

  if (project_id > 0)
    list = gitlab_issues_list_for_project (issues, project_id, NULL);
  else
    list = gitlab_issues_list_for_project_name (issues, project_name, NULL);

  return dex_future_then (gitlab_list_model_populate_all (list),
                          list_all_issues, NULL, NULL);
}

static DexFuture *
exit_main_loop (DexFuture *completed,
                gpointer   user_data)
{
  g_main_loop_quit (user_data);
  done = TRUE;
  return NULL;
}

int
main (int argc,
      char *argv[])
{
  g_autoptr(GOptionContext) context = g_option_context_new ("- gitlab issues tool");
  g_autoptr(GMainLoop) main_loop = g_main_loop_new (NULL, FALSE);
  g_autoptr(GError) error = NULL;
  g_autofree char *api_key = NULL;
  g_autofree char *host = NULL;
  g_autofree char *project_name = NULL;
  int project_id = 0;
  GOptionEntry entries[] = {
    { "project-id", 'p', 0, G_OPTION_ARG_INT, &project_id, "Project ID such as 29370" },
    { "project-name", 'P', 0, G_OPTION_ARG_STRING, &project_name, "Project name such as chergert/gitlab-glib", "PROJECT" },
    { "host", 'h', 0, G_OPTION_ARG_STRING, &host },
    { "api-key", 'k', 0, G_OPTION_ARG_STRING, &api_key },
    { 0 }
  };

  g_option_context_add_main_entries (context, entries, NULL);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("Error: %s\n", error->message);
      return EXIT_FAILURE;
    }

  if (host == NULL)
    {
      g_printerr ("Please provide a host name! (Example: gitlab.gnome.org)\n");
      return EXIT_FAILURE;
    }

  if (api_key == NULL)
    {
      g_printerr ("Please provide an API key!\n");
      return EXIT_FAILURE;
    }

  if (project_id == 0 && project_name == NULL)
    {
      g_printerr ("Please provide project id or name (-p or -P)!\n");
      return EXIT_FAILURE;
    }

  dex_future_disown (dex_future_finally (issues (host, g_strstrip (api_key), project_id, project_name),
                                         exit_main_loop,
                                         main_loop,
                                         NULL));

  if (!done)
    g_main_loop_run (main_loop);

  return 0;
}
