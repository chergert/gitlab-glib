/* gitlab-offset-list-model.c
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "gitlab-client.h"
#include "gitlab-list-model-private.h"
#include "gitlab-offset-list-model-private.h"
#include "gitlab-page-list-model-private.h"
#include "gitlab-resource.h"
#include "gitlab-utils-private.h"

struct _GitlabOffsetListModel
{
  GitlabListModel   parent_instance;
  GitlabClient     *client;
  char             *path;
  char            **params;
  GPtrArray        *pages;
  guint             page_size;
  guint             n_items;
};

struct _GitlabOffsetListModelClass
{
  GitlabListModelClass parent_class;
};

enum {
  PROP_0,
  PROP_CLIENT,
  PROP_PAGE_SIZE,
  PROP_PARAMS,
  PROP_PATH,
  N_PROPS
};

G_DEFINE_FINAL_TYPE (GitlabOffsetListModel, gitlab_offset_list_model, GITLAB_TYPE_LIST_MODEL)

static GParamSpec *properties[N_PROPS];

static GitlabPageListModel *
get_page_from_page_number (GitlabOffsetListModel *self,
                           guint                  page_number)
{
  GitlabPageListModel *page;

  g_assert (GITLAB_IS_OFFSET_LIST_MODEL (self));

  if (page_number >= self->pages->len)
    g_ptr_array_set_size (self->pages, page_number + 1);

  if (!(page = g_ptr_array_index (self->pages, page_number)))
    {
      g_auto(GStrv) params = NULL;
      guint next;
      guint n_items;

      params = g_strdupv ((char **)self->params);
      params = _gitlab_params_set_formatted (params, "page", "%u", page_number + 1);
      params = _gitlab_params_set_formatted (params, "per_page", "%u", self->page_size);

      next = (page_number + 1) * self->page_size;

      if (next > self->n_items)
        n_items = self->n_items % self->page_size;
      else
        n_items = self->page_size;

      page = gitlab_page_list_model_new (GITLAB_LIST_MODEL (self)->resource_type,
                                         self->client,
                                         self->path,
                                         (const char * const *)params,
                                         n_items);

      g_ptr_array_index (self->pages, page_number) = page;

      dex_future_disown (gitlab_page_list_model_await (page));
    }

  return page;
}

static GitlabPageListModel *
get_page_for_position (GitlabOffsetListModel *self,
                       guint                  position,
                       guint                 *offset)
{
  guint page_number;

  g_assert (GITLAB_IS_OFFSET_LIST_MODEL (self));
  g_assert (self->page_size > 0);
  g_assert (offset != NULL);

  page_number = position / self->page_size;

  *offset = position - (page_number * self->page_size);

  return get_page_from_page_number (self, page_number);
}

static gpointer
gitlab_offset_list_model_get_item (GitlabListModel *list_model,
                                   guint            position)
{
  GitlabOffsetListModel *self = (GitlabOffsetListModel *)list_model;
  GitlabPageListModel *page;
  guint offset;

  g_assert (GITLAB_IS_OFFSET_LIST_MODEL (self));

  page = get_page_for_position (self, position, &offset);

  return g_list_model_get_item (G_LIST_MODEL (page), offset);
}

static guint
gitlab_offset_list_model_get_n_items (GitlabListModel *list_model)
{
  GitlabOffsetListModel *self = GITLAB_OFFSET_LIST_MODEL (list_model);

  g_assert (GITLAB_IS_OFFSET_LIST_MODEL (self));

  return self->n_items;
}

static DexFuture *
gitlab_offset_list_model_await_all_pages (DexFuture *completed,
                                          gpointer   user_data)
{
  GitlabOffsetListModel *self = user_data;
  g_autoptr(GPtrArray) futures = NULL;
  GitlabPageListModel *first;
  guint x_total;
  guint n_pages;

  g_assert (GITLAB_IS_OFFSET_LIST_MODEL (self));
  g_assert (self->pages != NULL);
  g_assert (self->pages->len > 0);

  first = g_ptr_array_index (self->pages, 0);
  gitlab_page_list_model_get_x_total (first, &x_total);
  n_pages = (x_total / self->page_size) + !!(x_total % self->page_size);

  futures = g_ptr_array_new_with_free_func (dex_unref);
  for (guint page_number = 1; page_number < n_pages; page_number++)
    g_ptr_array_add (futures,
                     gitlab_page_list_model_await (get_page_from_page_number (self, page_number)));

  if (futures->len > 0)
    return dex_future_allv ((DexFuture **)futures->pdata, futures->len);

  return dex_future_new_for_boolean (TRUE);
}

static DexFuture *
gitlab_offset_list_model_await_finished (DexFuture *completed,
                                         gpointer   user_data)
{
  GitlabOffsetListModel *self = user_data;

  g_assert (GITLAB_IS_OFFSET_LIST_MODEL (self));

  return dex_future_new_take_object (g_object_ref (self));
}

static DexFuture *
gitlab_offset_list_model_populate_all (GitlabListModel *list_model)
{
  GitlabOffsetListModel *self = (GitlabOffsetListModel *)list_model;
  GitlabPageListModel *first;
  DexFuture *future;

  g_assert (GITLAB_IS_OFFSET_LIST_MODEL (self));

  /* Await the first page so that we know how many pages there
   * will be in total. After that, we can populate the rest of
   * the pages concurrently.
   */

  first = get_page_from_page_number (self, 0);

  future = dex_future_then (gitlab_page_list_model_await (first),
                            gitlab_offset_list_model_await_all_pages,
                            g_object_ref (self),
                            g_object_unref);
  future = dex_future_then (future,
                            gitlab_offset_list_model_await_finished,
                            g_object_ref (self),
                            g_object_unref);

  return future;
}

static void
gitlab_offset_list_model_dispose (GObject *object)
{
  GitlabOffsetListModel *self = (GitlabOffsetListModel *)object;

  g_clear_object (&self->client);
  g_clear_pointer (&self->path, g_free);
  g_clear_pointer (&self->pages, g_ptr_array_unref);
  g_clear_pointer (&self->params, g_strfreev);

  G_OBJECT_CLASS (gitlab_offset_list_model_parent_class)->dispose (object);
}

static void
gitlab_offset_list_model_get_property (GObject    *object,
                                       guint       prop_id,
                                       GValue     *value,
                                       GParamSpec *pspec)
{
  GitlabOffsetListModel *self = GITLAB_OFFSET_LIST_MODEL (object);

  switch (prop_id)
    {
    case PROP_CLIENT:
      g_value_set_object (value, self->client);
      break;

    case PROP_PAGE_SIZE:
      g_value_set_uint (value, self->page_size);
      break;

    case PROP_PARAMS:
      g_value_set_boxed (value, self->params);
      break;

    case PROP_PATH:
      g_value_set_string (value, self->path);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_offset_list_model_set_property (GObject      *object,
                                       guint         prop_id,
                                       const GValue *value,
                                       GParamSpec   *pspec)
{
  GitlabOffsetListModel *self = GITLAB_OFFSET_LIST_MODEL (object);

  switch (prop_id)
    {
    case PROP_CLIENT:
      self->client = g_value_dup_object (value);
      break;

    case PROP_PAGE_SIZE:
      self->page_size = g_value_get_uint (value);
      break;

    case PROP_PARAMS:
      self->params = g_value_dup_boxed (value);
      break;

    case PROP_PATH:
      self->path = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_offset_list_model_class_init (GitlabOffsetListModelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GitlabListModelClass *list_model_class = GITLAB_LIST_MODEL_CLASS (klass);

  object_class->dispose = gitlab_offset_list_model_dispose;
  object_class->get_property = gitlab_offset_list_model_get_property;
  object_class->set_property = gitlab_offset_list_model_set_property;

  list_model_class->get_n_items = gitlab_offset_list_model_get_n_items;
  list_model_class->get_item = gitlab_offset_list_model_get_item;
  list_model_class->populate_all = gitlab_offset_list_model_populate_all;

  properties[PROP_CLIENT] =
    g_param_spec_object ("client", NULL, NULL,
                         GITLAB_TYPE_CLIENT,
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_STATIC_STRINGS));

  properties[PROP_PAGE_SIZE] =
    g_param_spec_uint ("page-size", NULL, NULL,
                       1, 100, 20,
                       (G_PARAM_READWRITE |
                        G_PARAM_CONSTRUCT_ONLY |
                        G_PARAM_STATIC_STRINGS));

  properties[PROP_PARAMS] =
    g_param_spec_boxed ("params", NULL, NULL,
                        G_TYPE_STRV,
                        (G_PARAM_READWRITE |
                         G_PARAM_CONSTRUCT_ONLY |
                         G_PARAM_STATIC_STRINGS));

  properties[PROP_PATH] =
    g_param_spec_string ("path", NULL, NULL,
                         NULL,
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
gitlab_offset_list_model_init (GitlabOffsetListModel *self)
{
  self->page_size = 20;
  self->pages = g_ptr_array_new_with_free_func (g_object_unref);
}

GitlabListModel *
gitlab_offset_list_model_new (GType                resource_type,
                              GitlabClient        *client,
                              const char          *path,
                              const char * const  *params,
                              GitlabPageListModel *first_page,
                              guint                page_size,
                              guint                n_items)
{
  GitlabOffsetListModel *self;

  g_return_val_if_fail (GITLAB_IS_CLIENT (client), NULL);
  g_return_val_if_fail (path != NULL, NULL);
  g_return_val_if_fail (GITLAB_IS_PAGE_LIST_MODEL (first_page), NULL);
  g_return_val_if_fail (g_type_is_a (resource_type, GITLAB_TYPE_RESOURCE), NULL);
  g_return_val_if_fail (resource_type != GITLAB_TYPE_RESOURCE, NULL);

  self = g_object_new (GITLAB_TYPE_OFFSET_LIST_MODEL,
                       "resource-type", resource_type,
                       "page-size", page_size,
                       "client", client,
                       "params", params,
                       "path", path,
                       NULL);

  self->n_items = n_items;

  g_ptr_array_add (self->pages, g_object_ref (first_page));

  return GITLAB_LIST_MODEL (self);
}
