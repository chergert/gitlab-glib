/* gitlab-issues.h
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined (GITLAB_INSIDE) && !defined (GITLAB_COMPILATION)
# error "Only <gitlab-glib.h> can be included directly."
#endif

#include "gitlab-types.h"

G_BEGIN_DECLS

#define GITLAB_TYPE_ISSUES            (gitlab_issues_get_type())
#define GITLAB_ISSUES(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GITLAB_TYPE_ISSUES, GitlabIssues))
#define GITLAB_ISSUES_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GITLAB_TYPE_ISSUES, GitlabIssues const))
#define GITLAB_ISSUES_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GITLAB_TYPE_ISSUES, GitlabIssuesClass))
#define GITLAB_IS_ISSUES(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GITLAB_TYPE_ISSUES))
#define GITLAB_IS_ISSUES_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GITLAB_TYPE_ISSUES))
#define GITLAB_ISSUES_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GITLAB_TYPE_ISSUES, GitlabIssuesClass))

GITLAB_AVAILABLE_IN_ALL
GType            gitlab_issues_get_type              (void) G_GNUC_CONST;
GITLAB_AVAILABLE_IN_ALL
GitlabIssues    *gitlab_issues_new                   (GitlabClient      *client);
GITLAB_AVAILABLE_IN_ALL
GitlabListModel *gitlab_issues_list_for_project      (GitlabIssues      *self,
                                                      gint64             project_id,
                                                      GitlabIssueFilter *filter);
GITLAB_AVAILABLE_IN_ALL
GitlabListModel *gitlab_issues_list_for_project_name (GitlabIssues      *self,
                                                      const char        *project_name,
                                                      GitlabIssueFilter *filter);

G_END_DECLS
