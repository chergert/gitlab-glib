/* gitlab-issue.h
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined (GITLAB_INSIDE) && !defined (GITLAB_COMPILATION)
# error "Only <gitlab-glib.h> can be included directly."
#endif

#include "gitlab-types.h"

G_BEGIN_DECLS

#define GITLAB_TYPE_ISSUE            (gitlab_issue_get_type())
#define GITLAB_ISSUE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GITLAB_TYPE_ISSUE, GitlabIssue))
#define GITLAB_ISSUE_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GITLAB_TYPE_ISSUE, GitlabIssue const))
#define GITLAB_ISSUE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GITLAB_TYPE_ISSUE, GitlabIssueClass))
#define GITLAB_IS_ISSUE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GITLAB_TYPE_ISSUE))
#define GITLAB_IS_ISSUE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GITLAB_TYPE_ISSUE))
#define GITLAB_ISSUE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GITLAB_TYPE_ISSUE, GitlabIssueClass))

GITLAB_AVAILABLE_IN_ALL
GType       gitlab_issue_get_type         (void) G_GNUC_CONST;
GITLAB_AVAILABLE_IN_ALL
gint64      gitlab_issue_get_id           (GitlabIssue *self);
GITLAB_AVAILABLE_IN_ALL
gboolean    gitlab_issue_get_confidential (GitlabIssue *self);
GITLAB_AVAILABLE_IN_ALL
const char *gitlab_issue_get_description  (GitlabIssue *self);
GITLAB_AVAILABLE_IN_ALL
const char *gitlab_issue_get_title        (GitlabIssue *self);
GITLAB_AVAILABLE_IN_ALL
GDateTime  *gitlab_issue_dup_created_at   (GitlabIssue *self);
GITLAB_AVAILABLE_IN_ALL
GDateTime  *gitlab_issue_dup_due_date     (GitlabIssue *self);
GITLAB_AVAILABLE_IN_ALL
GDateTime  *gitlab_issue_dup_updated_at   (GitlabIssue *self);
GITLAB_AVAILABLE_IN_ALL
GDateTime  *gitlab_issue_dup_closed_at    (GitlabIssue *self);
GITLAB_AVAILABLE_IN_ALL
const char *gitlab_issue_get_web_url      (GitlabIssue *self);
GITLAB_AVAILABLE_IN_ALL
const char *gitlab_issue_get_state        (GitlabIssue *self);

G_END_DECLS
