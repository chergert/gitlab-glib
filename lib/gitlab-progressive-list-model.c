/* gitlab-progressive-list-model.c
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include <gtk/gtk.h>

#include "gitlab-client.h"
#include "gitlab-list-model-private.h"
#include "gitlab-page-list-model-private.h"
#include "gitlab-progressive-list-model-private.h"
#include "gitlab-resource.h"

struct _GitlabProgressiveListModel
{
  GitlabListModel       parent_instance;
  GitlabClient         *client;
  char                 *path;
  char                **params;
  GListStore           *pages;
  GtkFlattenListModel  *flatten;
  guint                 page_size;
};

struct _GitlabProgressiveListModelClass
{
  GitlabListModelClass parent_instance;
};

G_DEFINE_FINAL_TYPE (GitlabProgressiveListModel, gitlab_progressive_list_model, GITLAB_TYPE_LIST_MODEL)

enum {
  PROP_0,
  PROP_CLIENT,
  PROP_PAGE_SIZE,
  PROP_PATH,
  N_PROPS
};

static GParamSpec *properties[N_PROPS];

static gpointer
gitlab_progressive_list_model_get_item (GitlabListModel *list_model,
                                        guint            position)
{
  GitlabProgressiveListModel *self = (GitlabProgressiveListModel *)list_model;

  g_assert (GITLAB_IS_PROGRESSIVE_LIST_MODEL (self));

  return g_list_model_get_item (G_LIST_MODEL (self->flatten), position);
}

static guint
gitlab_progressive_list_model_get_n_items (GitlabListModel *list_model)
{
  GitlabProgressiveListModel *self = GITLAB_PROGRESSIVE_LIST_MODEL (list_model);

  g_assert (GITLAB_IS_PROGRESSIVE_LIST_MODEL (self));

  return g_list_model_get_n_items (G_LIST_MODEL (self->flatten));
}

static DexFuture *
gitlab_progressive_list_model_populate_all_fiber (gpointer user_data)
{
  GitlabProgressiveListModel *self = user_data;

  g_assert (GITLAB_IS_PROGRESSIVE_LIST_MODEL (self));

  for (;;)
    {
      g_autoptr(GitlabPageListModel) last = NULL;
      guint n_pages;

      /* Make sure we have at least one page */
      n_pages = g_list_model_get_n_items (G_LIST_MODEL (self->pages));
      if (n_pages == 0)
        break;

      /* Await on the last page being fetched */
      last = g_list_model_get_item (G_LIST_MODEL (self->pages), n_pages-1);
      if (!dex_await (gitlab_page_list_model_await (last), NULL))
        break;

      /* If this is the last page, then we're done */
      if (gitlab_page_list_model_is_last (last))
        break;

      /* Mark the page as accessed so the next is loaded */
      gitlab_page_list_model_set_accessed (last);
    }

  return dex_future_new_for_boolean (TRUE);
}

static DexFuture *
gitlab_progressive_list_model_populate_all (GitlabListModel *list_model)
{
  GitlabProgressiveListModel *self = (GitlabProgressiveListModel *)list_model;

  g_assert (GITLAB_IS_PROGRESSIVE_LIST_MODEL (self));

  return dex_scheduler_spawn (NULL, 0,
                              gitlab_progressive_list_model_populate_all_fiber,
                              g_object_ref (self),
                              g_object_unref);
}

static void
gitlab_progressive_list_model_items_changed_cb (GitlabProgressiveListModel *self,
                                                guint                       position,
                                                guint                       removed,
                                                guint                       added,
                                                GListModel                 *model)
{
  g_assert (GITLAB_IS_PROGRESSIVE_LIST_MODEL (self));
  g_assert (G_IS_LIST_MODEL (model));

  g_list_model_items_changed (G_LIST_MODEL (self), position, removed, added);

  if (removed != added)
    _gitlab_list_model_notify_n_items (GITLAB_LIST_MODEL (self));
}

static void
gitlab_progressive_list_model_dispose (GObject *object)
{
  GitlabProgressiveListModel *self = (GitlabProgressiveListModel *)object;

  g_clear_object (&self->client);
  g_clear_object (&self->pages);
  g_clear_pointer (&self->path, g_free);
  g_clear_pointer (&self->params, g_strfreev);

  G_OBJECT_CLASS (gitlab_progressive_list_model_parent_class)->dispose (object);
}

static void
gitlab_progressive_list_model_get_property (GObject    *object,
                                            guint       prop_id,
                                            GValue     *value,
                                            GParamSpec *pspec)
{
  GitlabProgressiveListModel *self = GITLAB_PROGRESSIVE_LIST_MODEL (object);

  switch (prop_id)
    {
    case PROP_CLIENT:
      g_value_set_object (value, self->client);
      break;

    case PROP_PAGE_SIZE:
      g_value_set_uint (value, self->page_size);
      break;

    case PROP_PATH:
      g_value_set_string (value, self->path);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_progressive_list_model_set_property (GObject      *object,
                                            guint         prop_id,
                                            const GValue *value,
                                            GParamSpec   *pspec)
{
  GitlabProgressiveListModel *self = GITLAB_PROGRESSIVE_LIST_MODEL (object);

  switch (prop_id)
    {
    case PROP_CLIENT:
      self->client = g_value_dup_object (value);
      break;

    case PROP_PAGE_SIZE:
      self->page_size = g_value_get_uint (value);
      break;

    case PROP_PATH:
      self->path = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_progressive_list_model_class_init (GitlabProgressiveListModelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GitlabListModelClass *list_model_class = GITLAB_LIST_MODEL_CLASS (klass);

  object_class->dispose = gitlab_progressive_list_model_dispose;
  object_class->get_property = gitlab_progressive_list_model_get_property;
  object_class->set_property = gitlab_progressive_list_model_set_property;

  list_model_class->get_n_items = gitlab_progressive_list_model_get_n_items;
  list_model_class->get_item = gitlab_progressive_list_model_get_item;
  list_model_class->populate_all = gitlab_progressive_list_model_populate_all;

  properties[PROP_CLIENT] =
    g_param_spec_object ("client", NULL, NULL,
                         GITLAB_TYPE_CLIENT,
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_STATIC_STRINGS));

  properties[PROP_PAGE_SIZE] =
    g_param_spec_uint ("page-size", NULL, NULL,
                       1, 100, 20,
                       (G_PARAM_READWRITE |
                        G_PARAM_CONSTRUCT_ONLY |
                        G_PARAM_STATIC_STRINGS));

  properties[PROP_PATH] =
    g_param_spec_string ("path", NULL, NULL,
                         NULL,
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
gitlab_progressive_list_model_init (GitlabProgressiveListModel *self)
{
  self->pages = g_list_store_new (GITLAB_TYPE_PAGE_LIST_MODEL);
  self->flatten = gtk_flatten_list_model_new (g_object_ref (G_LIST_MODEL (self->pages)));

  g_signal_connect_object (self->flatten,
                           "items-changed",
                           G_CALLBACK (gitlab_progressive_list_model_items_changed_cb),
                           self,
                           G_CONNECT_SWAPPED);
}

static void
gitlab_progressive_list_model_notify_was_accessed_cb (GitlabProgressiveListModel *self,
                                                      GParamSpec                 *pspec,
                                                      GitlabPageListModel        *page)
{
  g_autoptr(GitlabPageListModel) next_page = NULL;

  g_assert (GITLAB_IS_PROGRESSIVE_LIST_MODEL (self));
  g_assert (GITLAB_IS_PAGE_LIST_MODEL (page));

  if ((next_page = gitlab_page_list_model_next (page)))
    {
      g_signal_connect_object (next_page,
                               "notify::was-accessed",
                               G_CALLBACK (gitlab_progressive_list_model_notify_was_accessed_cb),
                               self,
                               G_CONNECT_SWAPPED);
      g_list_store_append (self->pages, next_page);
      dex_future_disown (gitlab_page_list_model_await (next_page));
    }
}

GitlabListModel *
gitlab_progressive_list_model_new (GType                item_type,
                                   GitlabClient        *client,
                                   const char          *path,
                                   const char * const  *params,
                                   GitlabPageListModel *first_page)
{
  GitlabProgressiveListModel *self;

  g_return_val_if_fail (GITLAB_IS_CLIENT (client), NULL);
  g_return_val_if_fail (path != NULL, NULL);
  g_return_val_if_fail (g_type_is_a (item_type, GITLAB_TYPE_RESOURCE), NULL);
  g_return_val_if_fail (item_type != GITLAB_TYPE_RESOURCE, NULL);
  g_return_val_if_fail (gitlab_page_list_model_was_accessed (first_page) == FALSE, NULL);

  self = g_object_new (GITLAB_TYPE_PROGRESSIVE_LIST_MODEL,
                       "resource-type", item_type,
                       NULL);
  self->client = g_object_ref (client);
  self->path = g_strdup (path);
  self->params = g_strdupv ((char **)params);

  g_signal_connect_object (first_page,
                           "notify::was-accessed",
                           G_CALLBACK (gitlab_progressive_list_model_notify_was_accessed_cb),
                           self,
                           G_CONNECT_SWAPPED);

  g_list_store_append (self->pages, first_page);

  dex_future_disown (gitlab_page_list_model_await (first_page));

  return GITLAB_LIST_MODEL (self);
}
