/* gitlab-service.h
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined (GITLAB_INSIDE) && !defined (GITLAB_COMPILATION)
# error "Only <gitlab.h> can be included directly."
#endif

#include "gitlab-types.h"

G_BEGIN_DECLS

#define GITLAB_TYPE_SERVICE            (gitlab_service_get_type())
#define GITLAB_SERVICE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GITLAB_TYPE_SERVICE, GitlabService))
#define GITLAB_SERVICE_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GITLAB_TYPE_SERVICE, GitlabService const))
#define GITLAB_SERVICE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GITLAB_TYPE_SERVICE, GitlabServiceClass))
#define GITLAB_IS_SERVICE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GITLAB_TYPE_SERVICE))
#define GITLAB_IS_SERVICE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GITLAB_TYPE_SERVICE))
#define GITLAB_SERVICE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GITLAB_TYPE_SERVICE, GitlabServiceClass))

GITLAB_AVAILABLE_IN_ALL
GType         gitlab_service_get_type   (void) G_GNUC_CONST;
GITLAB_AVAILABLE_IN_ALL
GitlabClient *gitlab_service_get_client (GitlabService *self);

G_END_DECLS
