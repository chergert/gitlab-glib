/* gitlab-list-item.h
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined (GITLAB_INSIDE) && !defined (GITLAB_COMPILATION)
# error "Only <gitlab-gtlib.h> can be included directly."
#endif

#include <glib-object.h>

#include "gitlab-version-macros.h"

G_BEGIN_DECLS

#define GITLAB_TYPE_LIST_ITEM (gitlab_list_item_get_type())

GITLAB_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (GitlabListItem, gitlab_list_item, GITLAB, LIST_ITEM, GObject)

GITLAB_AVAILABLE_IN_ALL
gpointer gitlab_list_item_get_item (GitlabListItem *self);

G_END_DECLS
