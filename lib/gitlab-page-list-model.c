/* gitlab-page-list-model.c
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include <errno.h>

#include <json-glib/json-glib.h>
#include <libsoup/soup.h>

#include "gitlab-client-private.h"
#include "gitlab-json-private.h"
#include "gitlab-list-item-private.h"
#include "gitlab-page-list-model-private.h"
#include "gitlab-resource-private.h"

struct _GitlabPageListModel
{
  GObject        parent_instance;

  GType          resource_type;

  GitlabClient  *client;
  char          *path;
  char         **params;
  GPtrArray     *items;
  DexFuture     *fetch;

  gint64         x_total;
  gint64         x_next_page;

  guint          known_size;

  guint          x_total_set : 1;
  guint          x_next_page_set : 1;
  guint          was_accessed : 1;
};

struct _GitlabPageListModelClass
{
  GObjectClass parent_class;
};

enum {
  PROP_0,
  PROP_RESOURCE_TYPE,
  PROP_WAS_ACCESSED,
  N_PROPS
};

static GParamSpec *properties[N_PROPS];

static guint
gitlab_page_list_model_get_n_items (GListModel *model)
{
  GitlabPageListModel *self = GITLAB_PAGE_LIST_MODEL (model);

  if (self->known_size)
    return self->known_size;

  return self->items->len;
}

static GType
gitlab_page_list_model_get_item_type (GListModel *model)
{
  return GITLAB_TYPE_LIST_ITEM;
}

static gpointer
gitlab_page_list_model_get_item (GListModel *model,
                                 guint       position)
{
  GitlabPageListModel *self = GITLAB_PAGE_LIST_MODEL (model);

  gitlab_page_list_model_set_accessed (self);

  if (self->known_size > 0 && position < self->known_size)
    {
      if (position >= self->items->len)
        g_ptr_array_set_size (self->items, position+1);

      if (g_ptr_array_index (self->items, position) == NULL)
        {
          GObject *list_item = g_object_new (GITLAB_TYPE_LIST_ITEM, NULL);
          g_ptr_array_index (self->items, position) = list_item;
        }
    }

  if (position >= self->items->len)
    return NULL;

  return g_object_ref (g_ptr_array_index (self->items, position));
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  iface->get_n_items = gitlab_page_list_model_get_n_items;
  iface->get_item_type = gitlab_page_list_model_get_item_type;
  iface->get_item = gitlab_page_list_model_get_item;
}

G_DEFINE_FINAL_TYPE_WITH_CODE (GitlabPageListModel, gitlab_page_list_model, G_TYPE_OBJECT,
                               G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_model_iface_init))

static void
gitlab_page_list_model_dispose (GObject *object)
{
  GitlabPageListModel *self = (GitlabPageListModel *)object;

  g_clear_object (&self->client);
  g_clear_pointer (&self->path, g_free);
  g_clear_pointer (&self->params, g_strfreev);
  g_clear_pointer (&self->items, g_ptr_array_unref);
  dex_clear (&self->fetch);

  G_OBJECT_CLASS (gitlab_page_list_model_parent_class)->dispose (object);
}

static void
gitlab_page_list_model_get_property (GObject    *object,
                                     guint       prop_id,
                                     GValue     *value,
                                     GParamSpec *pspec)
{
  GitlabPageListModel *self = GITLAB_PAGE_LIST_MODEL (object);

  switch (prop_id)
    {
    case PROP_RESOURCE_TYPE:
      g_value_set_gtype (value, self->resource_type);
      break;

    case PROP_WAS_ACCESSED:
      g_value_set_boolean (value, gitlab_page_list_model_was_accessed (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_page_list_model_set_property (GObject      *object,
                                     guint         prop_id,
                                     const GValue *value,
                                     GParamSpec   *pspec)
{
  GitlabPageListModel *self = GITLAB_PAGE_LIST_MODEL (object);

  switch (prop_id)
    {
    case PROP_RESOURCE_TYPE:
      self->resource_type = g_value_get_gtype (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_page_list_model_class_init (GitlabPageListModelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = gitlab_page_list_model_dispose;
  object_class->get_property = gitlab_page_list_model_get_property;
  object_class->set_property = gitlab_page_list_model_set_property;

  properties[PROP_RESOURCE_TYPE] =
    g_param_spec_gtype ("resource-type", NULL, NULL,
                        GITLAB_TYPE_RESOURCE,
                        (G_PARAM_READWRITE |
                         G_PARAM_CONSTRUCT_ONLY |
                         G_PARAM_STATIC_STRINGS));

  properties[PROP_WAS_ACCESSED] =
    g_param_spec_boolean ("was-accessed", NULL, NULL,
                          FALSE,
                          (G_PARAM_READABLE |
                           G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
gitlab_page_list_model_init (GitlabPageListModel *self)
{
  self->items = g_ptr_array_new_with_free_func (g_object_unref);
  self->resource_type = GITLAB_TYPE_RESOURCE;
}

GitlabPageListModel *
gitlab_page_list_model_new (GType               resource_type,
                            GitlabClient       *client,
                            const char         *path,
                            const char * const *params,
                            guint               known_size)
{
  GitlabPageListModel *self;

  g_return_val_if_fail (GITLAB_IS_CLIENT (client), NULL);
  g_return_val_if_fail (path != NULL, NULL);

  self = g_object_new (GITLAB_TYPE_PAGE_LIST_MODEL,
                       "resource-type", resource_type,
                       NULL);
  self->client = g_object_ref (client);
  self->path = g_strdup (path);
  self->params = g_strdupv ((char **)params);
  self->known_size = known_size;

  return self;
}

static void
gitlab_page_list_model_got_headers (GitlabPageListModel *self,
                                    SoupMessage         *message)
{
  SoupMessageHeaders *headers;
  const char *x_total;
  const char *x_next_page;

  g_assert (GITLAB_IS_PAGE_LIST_MODEL (self));
  g_assert (SOUP_IS_MESSAGE (message));

  headers = soup_message_get_response_headers (message);

  if ((x_total = soup_message_headers_get_one (headers, "x-total")))
    {
      self->x_total = g_ascii_strtoll (x_total, NULL, 10);
      self->x_total_set = TRUE;
    }

  if ((x_next_page = soup_message_headers_get_one (headers, "x-next-page")))
    {
      errno = 0;
      self->x_next_page = g_ascii_strtoll (x_next_page, NULL, 10);
      self->x_next_page_set = self->x_next_page > 0 && errno == 0;
    }
}

static DexFuture *
gitlab_page_list_model_fiber (gpointer user_data)
{
  GitlabPageListModel *self = user_data;
  g_autoptr(GMemoryOutputStream) memory_stream = NULL;
  g_autoptr(GInputStream) stream = NULL;
  g_autoptr(SoupMessage) message = NULL;
  g_autoptr(JsonParser) parser = NULL;
  g_autoptr(GError) error = NULL;
  g_autoptr(GBytes) bytes = NULL;
  JsonNode *root;
  gint64 n_copied;

  g_assert (GITLAB_IS_PAGE_LIST_MODEL (self));

  message = gitlab_client_create_message (self->client,
                                          "GET",
                                          self->path,
                                          (const char * const *)self->params,
                                          NULL);

  g_signal_connect_object (message,
                           "got-headers",
                           G_CALLBACK (gitlab_page_list_model_got_headers),
                           self,
                           G_CONNECT_SWAPPED);

  if (!(stream = dex_await_object (gitlab_client_send_message (self->client, message), &error)))
    return dex_future_new_for_error (g_steal_pointer (&error));

  /* Splice into local memory stream so we are no longer dealing with
   * callbacks into libsoup to parse.
   */
  memory_stream = G_MEMORY_OUTPUT_STREAM (g_memory_output_stream_new_resizable ());
  n_copied = dex_await_int64 (dex_output_stream_splice (G_OUTPUT_STREAM (memory_stream),
                                                        stream,
                                                        (G_OUTPUT_STREAM_SPLICE_CLOSE_SOURCE |
                                                         G_OUTPUT_STREAM_SPLICE_CLOSE_TARGET),
                                                        G_PRIORITY_DEFAULT),
                              &error);
  if (n_copied < 0)
    return dex_future_new_for_error (g_steal_pointer (&error));

  bytes = g_memory_output_stream_steal_as_bytes (memory_stream);
  if (!(parser = dex_await_object (gitlab_json_parse_bytes (bytes), &error)))
    return dex_future_new_for_error (g_steal_pointer (&error));

  if ((root = json_parser_get_root (parser)) &&
      JSON_NODE_HOLDS_ARRAY (root))
    {
      JsonArray *ar = json_node_get_array (root);
      guint length = json_array_get_length (ar);

      if (length > self->items->len)
        g_ptr_array_set_size (self->items, length);

      for (guint i = 0; i < length; i++)
        {
          JsonNode *node = json_array_get_element (ar, i);
          g_autoptr(GitlabResource) resource = NULL;
          GitlabListItem *list_item;

          resource = g_object_new (self->resource_type, NULL);
          _gitlab_resource_set_json (resource, node);

          if (!(list_item = g_ptr_array_index (self->items, i)))
            {
              list_item = g_object_new (GITLAB_TYPE_LIST_ITEM, NULL);
              g_ptr_array_index (self->items, i) = list_item;
            }

          _gitlab_list_item_set_item (list_item, resource);
        }

      if (self->known_size == 0)
        g_list_model_items_changed (G_LIST_MODEL (self), 0, 0, length);
    }

  return dex_future_new_for_boolean (TRUE);
}

DexFuture *
gitlab_page_list_model_await (GitlabPageListModel *self)
{
  g_return_val_if_fail (GITLAB_IS_PAGE_LIST_MODEL (self), NULL);

  if (self->fetch == NULL)
    self->fetch = dex_scheduler_spawn (NULL, 0,
                                       gitlab_page_list_model_fiber,
                                       g_object_ref (self),
                                       g_object_unref);

  return dex_ref (self->fetch);
}

gboolean
gitlab_page_list_model_get_x_total (GitlabPageListModel *self,
                                    guint               *x_total)
{
  g_return_val_if_fail (GITLAB_IS_PAGE_LIST_MODEL (self), 0);

  if (x_total != NULL)
    *x_total = CLAMP (self->x_total, 0, G_MAXUINT-1);

  return self->x_total_set;
}

GitlabPageListModel *
gitlab_page_list_model_next (GitlabPageListModel *self)
{
  g_return_val_if_fail (GITLAB_IS_PAGE_LIST_MODEL (self), NULL);

  if (self->x_next_page_set)
    {
      g_autofree char *page = g_strdup_printf ("page=%"G_GINT64_FORMAT, self->x_next_page);
      g_autoptr(GStrvBuilder) builder = g_strv_builder_new ();
      g_auto(GStrv) params = NULL;

      g_strv_builder_add (builder, page);

      if (self->params != NULL)
        {
          for (guint i = 0; self->params[i]; i++)
            {
              if (!g_str_has_prefix (self->params[i], "page="))
                g_strv_builder_add (builder, self->params[i]);
            }
        }

      params = g_strv_builder_end (builder);

      return gitlab_page_list_model_new (self->resource_type,
                                         self->client,
                                         self->path,
                                         (const char * const *)params,
                                         0);
    }

  return NULL;
}

gboolean
gitlab_page_list_model_is_last (GitlabPageListModel *self)
{
  g_return_val_if_fail (GITLAB_IS_PAGE_LIST_MODEL (self), FALSE);
  g_return_val_if_fail (self->fetch != NULL, FALSE);
  g_return_val_if_fail (dex_future_is_resolved (self->fetch), FALSE);

  return !self->x_next_page_set;
}

gboolean
gitlab_page_list_model_was_accessed (GitlabPageListModel *self)
{
  g_return_val_if_fail (GITLAB_IS_PAGE_LIST_MODEL (self), FALSE);

  return self->was_accessed;
}

void
gitlab_page_list_model_set_accessed (GitlabPageListModel *self)
{
  g_return_if_fail (GITLAB_IS_PAGE_LIST_MODEL (self));

  if (self->was_accessed == FALSE)
    {
      self->was_accessed = TRUE;
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_WAS_ACCESSED]);
    }
}
