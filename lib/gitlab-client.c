/* gitlab-client.c
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "gitlab-client-private.h"
#include "gitlab-secret-private.h"
#include "gitlab-service.h"

struct _GitlabClient
{
  GObject       parent_instance;
  SoupSession  *session;
  GitlabSecret *secret;
  char         *host;
  int           port;
};

struct _GitlabClientClass
{
  GObjectClass parent_class;
};

enum {
  PROP_0,
  PROP_HOST,
  PROP_PORT,
  PROP_SECRET,
  N_PROPS
};

G_DEFINE_FINAL_TYPE (GitlabClient, gitlab_client, G_TYPE_OBJECT)

static SoupSession *default_session;
static GParamSpec *properties[N_PROPS];

static void
gitlab_client_dispose (GObject *object)
{
  GitlabClient *self = (GitlabClient *)object;

  g_clear_object (&self->secret);
  g_clear_object (&self->session);
  g_clear_pointer (&self->host, g_free);

  G_OBJECT_CLASS (gitlab_client_parent_class)->dispose (object);
}

static void
gitlab_client_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  GitlabClient *self = GITLAB_CLIENT (object);

  switch (prop_id)
    {
    case PROP_HOST:
      g_value_set_string (value, gitlab_client_get_host (self));
      break;

    case PROP_PORT:
      g_value_set_int (value, gitlab_client_get_port (self));
      break;

    case PROP_SECRET:
      g_value_set_object (value, gitlab_client_get_secret (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_client_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  GitlabClient *self = GITLAB_CLIENT (object);

  switch (prop_id)
    {
    case PROP_HOST:
      gitlab_client_set_host (self, g_value_get_string (value));
      break;

    case PROP_PORT:
      gitlab_client_set_port (self, g_value_get_int (value));
      break;

    case PROP_SECRET:
      gitlab_client_set_secret (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_client_class_init (GitlabClientClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = gitlab_client_dispose;
  object_class->get_property = gitlab_client_get_property;
  object_class->set_property = gitlab_client_set_property;

  properties[PROP_HOST] =
    g_param_spec_string ("host", NULL, NULL,
                         NULL,
                         (G_PARAM_READWRITE |
                          G_PARAM_EXPLICIT_NOTIFY |
                          G_PARAM_STATIC_STRINGS));

  properties[PROP_PORT] =
    g_param_spec_int ("port", NULL, NULL,
                      1, 65535, 443,
                      (G_PARAM_READWRITE |
                       G_PARAM_EXPLICIT_NOTIFY |
                       G_PARAM_STATIC_STRINGS));

  properties[PROP_SECRET] =
    g_param_spec_object ("secret", NULL, NULL,
                         GITLAB_TYPE_SECRET,
                         (G_PARAM_READWRITE |
                          G_PARAM_EXPLICIT_NOTIFY |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
gitlab_client_init (GitlabClient *self)
{
  if (g_once_init_enter (&default_session))
    g_once_init_leave (&default_session, soup_session_new ());

  self->session = g_object_ref (default_session);
  self->host = g_strdup ("gitlab.com");
  self->port = 443;
}

/**
 * gitlab_client_new:
 * @host: (nullable): the hostname of the instance or %NULL
 * @secret: (nullable): the user secret or %NULL
 *
 * Creates a new #GitlabClient.
 *
 * Returns: (transfer full): a #GitlabClient
 */
GitlabClient *
gitlab_client_new (const char   *host,
                   GitlabSecret *secret)
{
  g_return_val_if_fail (!secret || GITLAB_IS_SECRET (secret), NULL);

  return g_object_new (GITLAB_TYPE_CLIENT,
                       "host", host,
                       "secret", secret,
                       NULL);
}

const char *
gitlab_client_get_host (GitlabClient *self)
{
  g_return_val_if_fail (GITLAB_IS_CLIENT (self), NULL);

  return self->host;
}

void
gitlab_client_set_host (GitlabClient *self,
                        const char   *host)
{
  g_return_if_fail (GITLAB_IS_CLIENT (self));

  if (host == NULL)
    host = "gitlab.com";

  if (g_set_str (&self->host, host))
    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_HOST]);
}

int
gitlab_client_get_port (GitlabClient *self)
{
  g_return_val_if_fail (GITLAB_IS_CLIENT (self), 0);

  return self->port;
}

void
gitlab_client_set_port (GitlabClient *self,
                        int           port)
{
  g_return_if_fail (GITLAB_IS_CLIENT (self));
  g_return_if_fail (port > 0);
  g_return_if_fail (port <= 65535);

  if (self->port != port)
    {
      self->port = port;
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_PORT]);
    }
}

/**
 * gitlab_client_get_secret:
 * @self: a #GitlabClient
 *
 * Sets the secret to use for API endpoints.
 *
 * Returns: (transfer none): a #GitlabSecret
 */
GitlabSecret *
gitlab_client_get_secret (GitlabClient *self)
{
  g_return_val_if_fail (GITLAB_IS_CLIENT (self), NULL);

  return self->secret;
}

void
gitlab_client_set_secret (GitlabClient *self,
                          GitlabSecret *secret)
{
  g_return_if_fail (GITLAB_IS_CLIENT (self));
  g_return_if_fail (!secret || GITLAB_IS_SECRET (secret));

  if (g_set_object (&self->secret, secret))
    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SECRET]);
}

SoupMessage *
gitlab_client_create_message (GitlabClient       *self,
                              const char         *method,
                              const char         *path,
                              const char * const *params,
                              ...)
{
  g_autofree char *uri_string = NULL;
  g_autoptr(GString) str = NULL;
  g_autoptr(GUri) uri = NULL;
  const char *key;
  va_list args;

  g_return_val_if_fail (GITLAB_IS_CLIENT (self), NULL);
  g_return_val_if_fail (method != NULL, NULL);
  g_return_val_if_fail (path != NULL, NULL);

  str = g_string_new (NULL);

  if (params != NULL)
    {
      for (guint i = 0; params[i]; i++)
        {
          const char *eq = strchr (params[i], '=');
          g_autofree char *pkey = g_strndup (params[i], eq - params[i]);
          g_autofree char *value = g_strdup (eq + 1);

          if (str->len > 0)
            g_string_append_c (str, '&');

          g_string_append_uri_escaped (str, pkey, NULL, TRUE);
          g_string_append_c (str, '=');
          g_string_append_uri_escaped (str, value, NULL, TRUE);
        }
    }

  va_start (args, params);
  key = va_arg (args, const char *);
  while (key != NULL)
    {
      const char *value = va_arg (args, const char *);

      if (str->len > 0)
        g_string_append_c (str, '&');

      g_string_append_uri_escaped (str, key, NULL, TRUE);
      g_string_append_c (str, '=');
      g_string_append_uri_escaped (str, value, NULL, TRUE);

      key = va_arg (args, const char *);
    }
  va_end (args);

  uri = g_uri_build (G_URI_FLAGS_NONE,
                     "https",
                     NULL,
                     self->host,
                     self->port,
                     path,
                     str->str,
                     NULL);
  uri_string = g_uri_to_string (uri);

  return soup_message_new (method, uri_string);
}

static void
gitlab_client_send_message_cb (GObject      *object,
                               GAsyncResult *result,
                               gpointer      user_data)
{
  SoupSession *session = (SoupSession *)object;
  g_autoptr(DexPromise) promise = user_data;
  GInputStream *stream;
  GError *error = NULL;

  g_assert (SOUP_IS_SESSION (session));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (DEX_IS_PROMISE (promise));

  if ((stream = soup_session_send_finish (session, result, &error)))
    dex_promise_resolve_object (promise, stream);
  else
    dex_promise_reject (promise, error);
}

DexFuture *
gitlab_client_send_message (GitlabClient *self,
                            SoupMessage  *message)
{
  DexPromise *promise;

  g_return_val_if_fail (GITLAB_IS_CLIENT (self), NULL);
  g_return_val_if_fail (SOUP_IS_MESSAGE (message), NULL);

  promise = dex_promise_new_cancellable ();

  if (self->secret != NULL)
    _gitlab_secret_sign (self->secret, message);

  g_object_set_data_full (G_OBJECT (message),
                          "SOUP_SESSION",
                          g_object_ref (self->session),
                          g_object_unref);

  soup_session_send_async (self->session,
                           message,
                           G_PRIORITY_DEFAULT,
                           dex_promise_get_cancellable (promise),
                           gitlab_client_send_message_cb,
                           dex_ref (promise));

  return DEX_FUTURE (promise);
}
