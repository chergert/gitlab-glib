/* gitlab-service.c
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "gitlab-service-private.h"

enum {
  PROP_0,
  PROP_CLIENT,
  N_PROPS
};

G_DEFINE_ABSTRACT_TYPE (GitlabService, gitlab_service, G_TYPE_OBJECT)

static GParamSpec *properties[N_PROPS];

static void
gitlab_service_dispose (GObject *object)
{
  GitlabService *self = (GitlabService *)object;

  g_clear_object (&self->client);

  G_OBJECT_CLASS (gitlab_service_parent_class)->dispose (object);
}

static void
gitlab_service_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  GitlabService *self = GITLAB_SERVICE (object);

  switch (prop_id)
    {
    case PROP_CLIENT:
      g_value_set_object (value, gitlab_service_get_client (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_service_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  GitlabService *self = GITLAB_SERVICE (object);

  switch (prop_id)
    {
    case PROP_CLIENT:
      self->client = g_value_dup_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_service_class_init (GitlabServiceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = gitlab_service_dispose;
  object_class->get_property = gitlab_service_get_property;
  object_class->set_property = gitlab_service_set_property;

  properties[PROP_CLIENT] =
    g_param_spec_object ("client", NULL, NULL,
                         GITLAB_TYPE_CLIENT,
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
gitlab_service_init (GitlabService *self)
{
}

/**
 * gitlab_service_get_client:
 * @self: a #GitlabService
 *
 * Gets the client used by the service.
 *
 * Returns: (transfer none): a #GitlabClient
 */
GitlabClient *
gitlab_service_get_client (GitlabService *self)
{
  g_return_val_if_fail (GITLAB_IS_SERVICE (self), NULL);

  return self->client;
}
