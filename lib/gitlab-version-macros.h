/*
 * gitlab-version-macros.h
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <glib.h>

#include "gitlab-version.h"

#ifndef _GITLAB_EXTERN
# define _GITLAB_EXTERN extern
#endif

#define GITLAB_VERSION_CUR_STABLE (G_ENCODE_VERSION (GITLAB_MAJOR_VERSION, 0))

#ifdef GITLAB_DISABLE_DEPRECATION_WARNINGS
# define GITLAB_DEPRECATED _GITLAB_EXTERN
# define GITLAB_DEPRECATED_FOR(f) _GITLAB_EXTERN
# define GITLAB_UNAVAILABLE(maj,min) _GITLAB_EXTERN
#else
# define GITLAB_DEPRECATED G_DEPRECATED _GITLAB_EXTERN
# define GITLAB_DEPRECATED_FOR(f) G_DEPRECATED_FOR (f) _GITLAB_EXTERN
# define GITLAB_UNAVAILABLE(maj,min) G_UNAVAILABLE (maj, min) _GITLAB_EXTERN
#endif

#define GITLAB_VERSION_1_0 (G_ENCODE_VERSION (1, 0))

#if GITLAB_MAJOR_VERSION == GITLAB_VERSION_1_0
# define GITLAB_VERSION_PREV_STABLE (GITLAB_VERSION_1_0)
#else
# define GITLAB_VERSION_PREV_STABLE (G_ENCODE_VERSION (GITLAB_MAJOR_VERSION - 1, 0))
#endif

/**
 * GITLAB_VERSION_MIN_REQUIRED:
 *
 * A macro that should be defined by the user prior to including
 * the gitlab.h header.
 *
 * The definition should be one of the predefined GITLAB version
 * macros: %GITLAB_VERSION_1_0, ...
 *
 * This macro defines the lower bound for the Builder API to use.
 *
 * If a function has been deprecated in a newer version of Builder,
 * it is possible to use this symbol to avoid the compiler warnings
 * without disabling warning for every deprecated function.
 */
#ifndef GITLAB_VERSION_MIN_REQUIRED
# define GITLAB_VERSION_MIN_REQUIRED (GITLAB_VERSION_CUR_STABLE)
#endif

/**
 * GITLAB_VERSION_MAX_ALLOWED:
 *
 * A macro that should be defined by the user prior to including
 * the gitlab.h header.

 * The definition should be one of the predefined Builder version
 * macros: %GITLAB_VERSION_1_0, %GITLAB_VERSION_1_2,...
 *
 * This macro defines the upper bound for the GITLAB API to use.
 *
 * If a function has been introduced in a newer version of Builder,
 * it is possible to use this symbol to get compiler warnings when
 * trying to use that function.
 */
#ifndef GITLAB_VERSION_MAX_ALLOWED
# if GITLAB_VERSION_MIN_REQUIRED > GITLAB_VERSION_PREV_STABLE
#  define GITLAB_VERSION_MAX_ALLOWED (GITLAB_VERSION_MIN_REQUIRED)
# else
#  define GITLAB_VERSION_MAX_ALLOWED (GITLAB_VERSION_CUR_STABLE)
# endif
#endif

#define GITLAB_AVAILABLE_IN_ALL _GITLAB_EXTERN

#if GITLAB_VERSION_MIN_REQUIRED >= GITLAB_VERSION_1_0
# define GITLAB_DEPRECATED_IN_1_0 GITLAB_DEPRECATED
# define GITLAB_DEPRECATED_IN_1_0_FOR(f) GITLAB_DEPRECATED_FOR(f)
#else
# define GITLAB_DEPRECATED_IN_1_0 _GITLAB_EXTERN
# define GITLAB_DEPRECATED_IN_1_0_FOR(f) _GITLAB_EXTERN
#endif
#if GITLAB_VERSION_MAX_ALLOWED < GITLAB_VERSION_1_0
# define GITLAB_AVAILABLE_IN_1_0 GITLAB_UNAVAILABLE(1, 0)
#else
# define GITLAB_AVAILABLE_IN_1_0 _GITLAB_EXTERN
#endif
