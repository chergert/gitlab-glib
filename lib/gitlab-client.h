/* gitlab-client.h
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined (GITLAB_INSIDE) && !defined (GITLAB_COMPILATION)
# error "Only <gitlab-glib.h> can be included directly."
#endif

#include "gitlab-types.h"

G_BEGIN_DECLS

#define GITLAB_TYPE_CLIENT            (gitlab_client_get_type())
#define GITLAB_CLIENT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GITLAB_TYPE_CLIENT, GitlabClient))
#define GITLAB_CLIENT_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GITLAB_TYPE_CLIENT, GitlabClient const))
#define GITLAB_CLIENT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GITLAB_TYPE_CLIENT, GitlabClientClass))
#define GITLAB_IS_CLIENT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GITLAB_TYPE_CLIENT))
#define GITLAB_IS_CLIENT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GITLAB_TYPE_CLIENT))
#define GITLAB_CLIENT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GITLAB_TYPE_CLIENT, GitlabClientClass))

GITLAB_AVAILABLE_IN_ALL
GType          gitlab_client_get_type       (void) G_GNUC_CONST;
GITLAB_AVAILABLE_IN_ALL
GitlabClient  *gitlab_client_new            (const char   *host,
                                             GitlabSecret *secret);
GITLAB_AVAILABLE_IN_ALL
const char    *gitlab_client_get_host       (GitlabClient *self);
GITLAB_AVAILABLE_IN_ALL
void           gitlab_client_set_host       (GitlabClient *self,
                                             const char   *host);
GITLAB_AVAILABLE_IN_ALL
int            gitlab_client_get_port       (GitlabClient *self);
GITLAB_AVAILABLE_IN_ALL
void           gitlab_client_set_port       (GitlabClient *self,
                                             int           port);
GITLAB_AVAILABLE_IN_ALL
GitlabSecret  *gitlab_client_get_secret     (GitlabClient *self);
GITLAB_AVAILABLE_IN_ALL
void           gitlab_client_set_secret     (GitlabClient *self,
                                             GitlabSecret *secret);

G_END_DECLS
