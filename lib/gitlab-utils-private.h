/* gitlab-utils-private.h
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

static inline gboolean
_g_set_int64 (gint64 *ptr,
              gint64  val)
{
  if (*ptr == val)
    return FALSE;
  *ptr = val;
  return TRUE;
}

static inline gboolean
_g_set_boolean (gboolean *ptr,
                gboolean  val)
{
  val = !!val;
  if (*ptr == val)
    return FALSE;
  *ptr = val;
  return TRUE;
}

char **_gitlab_params_set     (char        **params,
                               const char   *key,
                               const char   *value) G_GNUC_WARN_UNUSED_RESULT;
char **_gitlab_params_set_int (char        **params,
                               const char   *key,
                               gint64        value) G_GNUC_WARN_UNUSED_RESULT;

G_GNUC_WARN_UNUSED_RESULT
G_GNUC_PRINTF (3, 4)
static inline char **
_gitlab_params_set_formatted (char **params,
                              const char *key,
                              const char *format,
                              ...)
{
  g_autofree char *value = NULL;
  va_list args;

  va_start (args, format);
  value = g_strdup_vprintf (format, args);
  va_end (args);

  return _gitlab_params_set (params, key, value);
}

G_END_DECLS
