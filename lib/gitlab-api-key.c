/* gitlab-api-key.c
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "gitlab-api-key.h"
#include "gitlab-secret-private.h"

struct _GitlabApiKey
{
  GitlabSecret parent_instance;
  char *api_key;
};

struct _GitlabApiKeyClass
{
  GitlabSecretClass parent_class;
};

G_DEFINE_FINAL_TYPE (GitlabApiKey, gitlab_api_key, GITLAB_TYPE_SECRET)

static void
gitlab_api_key_sign (GitlabSecret *secret,
                     SoupMessage  *message)
{
  GitlabApiKey *self = (GitlabApiKey *)secret;
  SoupMessageHeaders *headers;

  g_assert (GITLAB_IS_API_KEY (self));
  g_assert (SOUP_IS_MESSAGE (message));

  if (self->api_key == NULL)
    return;

  headers = soup_message_get_request_headers (message);

  soup_message_headers_append (headers, "PRIVATE-TOKEN", self->api_key);
}

static void
gitlab_api_key_finalize (GObject *object)
{
  GitlabApiKey *self = (GitlabApiKey *)object;

  g_clear_pointer (&self->api_key, g_free);

  G_OBJECT_CLASS (gitlab_api_key_parent_class)->finalize (object);
}

static void
gitlab_api_key_class_init (GitlabApiKeyClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GitlabSecretClass *secret_class = GITLAB_SECRET_CLASS (klass);

  object_class->finalize = gitlab_api_key_finalize;

  secret_class->sign = gitlab_api_key_sign;
}

static void
gitlab_api_key_init (GitlabApiKey *self)
{
}

GitlabApiKey *
gitlab_api_key_new (const char *api_key)
{
  GitlabApiKey *self;

  self = g_object_new (GITLAB_TYPE_API_KEY, NULL);
  self->api_key = g_strdup (api_key);

  return self;
}

/**
 * gitlab_api_key_to_string:
 * @self: a #GitlabApiKey
 *
 * Gets the API key used for communicating with Gitlab.
 *
 * Returns: (transfer full): a string containing the api key
 */
char *
gitlab_api_key_to_string (GitlabApiKey *self)
{
  g_return_val_if_fail (GITLAB_IS_API_KEY (self), NULL);

  return g_strdup (self->api_key);
}
