/* gitlab-page-list-model-private.h
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <gio/gio.h>
#include <libdex.h>

#include "gitlab-types.h"

G_BEGIN_DECLS

#define GITLAB_TYPE_PAGE_LIST_MODEL            (gitlab_page_list_model_get_type())
#define GITLAB_PAGE_LIST_MODEL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GITLAB_TYPE_PAGE_LIST_MODEL, GitlabPageListModel))
#define GITLAB_PAGE_LIST_MODEL_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GITLAB_TYPE_PAGE_LIST_MODEL, GitlabPageListModel const))
#define GITLAB_PAGE_LIST_MODEL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GITLAB_TYPE_PAGE_LIST_MODEL, GitlabPageListModelClass))
#define GITLAB_IS_PAGE_LIST_MODEL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GITLAB_TYPE_PAGE_LIST_MODEL))
#define GITLAB_IS_PAGE_LIST_MODEL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GITLAB_TYPE_PAGE_LIST_MODEL))
#define GITLAB_PAGE_LIST_MODEL_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GITLAB_TYPE_PAGE_LIST_MODEL, GitlabPageListModelClass))

typedef struct _GitlabPageListModel GitlabPageListModel;
typedef struct _GitlabPageListModelClass GitlabPageListModelClass;

GType                gitlab_page_list_model_get_type      (void) G_GNUC_CONST;
GitlabPageListModel *gitlab_page_list_model_new           (GType                resource_type,
                                                           GitlabClient        *client,
                                                           const char          *path,
                                                           const char * const  *params,
                                                           guint                known_size);
GitlabPageListModel *gitlab_page_list_model_next          (GitlabPageListModel *self);
DexFuture           *gitlab_page_list_model_await         (GitlabPageListModel *self);
gboolean             gitlab_page_list_model_was_accessed  (GitlabPageListModel *self);
void                 gitlab_page_list_model_set_accessed  (GitlabPageListModel *self);
gboolean             gitlab_page_list_model_get_x_total   (GitlabPageListModel *self,
                                                           guint               *x_total);
gboolean             gitlab_page_list_model_is_last       (GitlabPageListModel *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (GitlabPageListModel, g_object_unref)

G_END_DECLS
