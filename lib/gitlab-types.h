/* gitlab-types.h
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined (GITLAB_INSIDE) && !defined (GITLAB_COMPILATION)
# error "Only <gitlab-glib.h> can be included directly."
#endif

#include <glib-object.h>

#include "gitlab-version-macros.h"

G_BEGIN_DECLS

typedef struct _GitlabApiKey GitlabApiKey;
typedef struct _GitlabApiKeyClass GitlabApiKeyClass;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (GitlabApiKey, g_object_unref)

typedef struct _GitlabClient GitlabClient;
typedef struct _GitlabClientClass GitlabClientClass;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (GitlabClient, g_object_unref)

typedef struct _GitlabFilter GitlabFilter;
typedef struct _GitlabFilterClass GitlabFilterClass;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (GitlabFilter, g_object_unref)

typedef struct _GitlabIssue GitlabIssue;
typedef struct _GitlabIssueClass GitlabIssueClass;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (GitlabIssue, g_object_unref)

typedef struct _GitlabIssues GitlabIssues;
typedef struct _GitlabIssuesClass GitlabIssuesClass;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (GitlabIssues, g_object_unref)

typedef struct _GitlabIssueFilter GitlabIssueFilter;
typedef struct _GitlabIssueFilterClass GitlabIssueFilterClass;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (GitlabIssueFilter, g_object_unref)

typedef struct _GitlabListModel GitlabListModel;
typedef struct _GitlabListModelClass GitlabListModelClass;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (GitlabListModel, g_object_unref)

typedef struct _GitlabProject GitlabProject;
typedef struct _GitlabProjectClass GitlabProjectClass;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (GitlabProject, g_object_unref)

typedef struct _GitlabProjects GitlabProjects;
typedef struct _GitlabProjectsClass GitlabProjectsClass;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (GitlabProjects, g_object_unref)

typedef struct _GitlabProjectFilter GitlabProjectFilter;
typedef struct _GitlabProjectFilterClass GitlabProjectFilterClass;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (GitlabProjectFilter, g_object_unref)

typedef struct _GitlabResource GitlabResource;
typedef struct _GitlabResourceClass GitlabResourceClass;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (GitlabResource, g_object_unref)

typedef struct _GitlabSecret GitlabSecret;
typedef struct _GitlabSecretClass GitlabSecretClass;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (GitlabSecret, g_object_unref)

typedef struct _GitlabService GitlabService;
typedef struct _GitlabServiceClass GitlabServiceClass;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (GitlabService, g_object_unref)

G_END_DECLS
