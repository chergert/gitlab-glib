/* gitlab-projects.c
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "gitlab-filter-private.h"
#include "gitlab-project-filter.h"
#include "gitlab-project.h"
#include "gitlab-projects.h"
#include "gitlab-service-private.h"
#include "gitlab-wrapped-list-model-private.h"

struct _GitlabProjects
{
  GitlabService parent_instance;
};

struct _GitlabProjectsClass
{
  GitlabServiceClass parent_class;
};

G_DEFINE_FINAL_TYPE (GitlabProjects, gitlab_projects, GITLAB_TYPE_SERVICE)

static void
gitlab_projects_class_init (GitlabProjectsClass *klass)
{
}

static void
gitlab_projects_init (GitlabProjects *self)
{
}

GitlabProjects *
gitlab_projects_new (GitlabClient *client)
{
  g_return_val_if_fail (GITLAB_IS_CLIENT (client), NULL);

  return g_object_new (GITLAB_TYPE_PROJECTS,
                       "client", client,
                       NULL);
}

/**
 * gitlab_projects_list:
 * @self: a #GitlabProjects
 * @filter: (nullable): optional search filters
 *
 * Returns: (transfer full): a new #GitlabListModel of projects
 */
GitlabListModel *
gitlab_projects_list (GitlabProjects      *self,
                      GitlabProjectFilter *filter)
{
  g_auto(GStrv) params = NULL;
  GitlabClient *client;

  g_return_val_if_fail (GITLAB_IS_PROJECTS (self), NULL);
  g_return_val_if_fail (!filter || GITLAB_IS_PROJECT_FILTER (filter), NULL);

  client = gitlab_service_get_client (GITLAB_SERVICE (self));

  if (filter != NULL)
    params = _gitlab_filter_to_params (GITLAB_FILTER (filter));

  return gitlab_wrapped_list_model_new (GITLAB_TYPE_PROJECT, client, "/api/v4/projects", (const char * const *)params);
}
