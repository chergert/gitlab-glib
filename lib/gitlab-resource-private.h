/* gitlab-resource-private.h
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <json-glib/json-glib.h>

#include "gitlab-resource.h"

G_BEGIN_DECLS

struct _GitlabResource
{
  GObject parent_instance;
  JsonNode *node;
};

struct _GitlabResourceClass
{
  GObjectClass parent_class;

  void (*set_json) (GitlabResource *self,
                    JsonNode       *node);
};

void        _gitlab_resource_set_json      (GitlabResource *self,
                                            JsonNode       *node);
gboolean    _gitlab_resource_get_bool      (GitlabResource *self,
                                            const char     *key);
gint64      _gitlab_resource_get_int       (GitlabResource *self,
                                            const char     *key);
const char *_gitlab_resource_get_string    (GitlabResource *self,
                                            const char     *key);
GDateTime  *_gitlab_resource_dup_date_time (GitlabResource *self,
                                            const char     *key);

G_END_DECLS
