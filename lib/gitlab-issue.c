/* gitlab-issue.c
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "gitlab-issue.h"
#include "gitlab-resource-private.h"

struct _GitlabIssue
{
  GitlabResource parent_instance;
};

struct _GitlabIssueClass
{
  GitlabResourceClass parent_class;
};

enum {
  PROP_0,
  PROP_CLOSED_AT,
  PROP_CONFIDENTIAL,
  PROP_CREATED_AT,
  PROP_DESCRIPTION,
  PROP_DUE_DATE,
  PROP_ID,
  PROP_STATE,
  PROP_TITLE,
  PROP_UPDATED_AT,
  PROP_WEB_URL,
  N_PROPS
};

G_DEFINE_FINAL_TYPE (GitlabIssue, gitlab_issue, GITLAB_TYPE_RESOURCE)

static GParamSpec *properties[N_PROPS];

static void
gitlab_issue_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  GitlabIssue *self = GITLAB_ISSUE (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_int64 (value, gitlab_issue_get_id (self));
      break;

    case PROP_CONFIDENTIAL:
      g_value_set_boolean (value, gitlab_issue_get_confidential (self));
      break;

    case PROP_CREATED_AT:
      g_value_take_boxed (value, gitlab_issue_dup_created_at (self));
      break;

    case PROP_CLOSED_AT:
      g_value_take_boxed (value, gitlab_issue_dup_closed_at (self));
      break;

    case PROP_DESCRIPTION:
      g_value_set_string (value, gitlab_issue_get_description (self));
      break;

    case PROP_DUE_DATE:
      g_value_take_boxed (value, gitlab_issue_dup_due_date (self));
      break;

    case PROP_TITLE:
      g_value_set_string (value, gitlab_issue_get_title (self));
      break;

    case PROP_UPDATED_AT:
      g_value_take_boxed (value, gitlab_issue_dup_updated_at (self));
      break;

    case PROP_WEB_URL:
      g_value_set_string (value, gitlab_issue_get_web_url (self));
      break;

    case PROP_STATE:
      g_value_set_string (value, gitlab_issue_get_state (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_issue_class_init (GitlabIssueClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = gitlab_issue_get_property;

  properties[PROP_ID] =
    g_param_spec_int64 ("id", NULL, NULL,
                        0, G_MAXINT64, 0,
                        (G_PARAM_READABLE |
                         G_PARAM_STATIC_STRINGS));

  properties[PROP_CLOSED_AT] =
    g_param_spec_boxed ("closed-at", NULL, NULL,
                         G_TYPE_DATE_TIME,
                         (G_PARAM_READABLE |
                          G_PARAM_STATIC_STRINGS));

  properties[PROP_CONFIDENTIAL] =
    g_param_spec_boolean ("confidential", NULL, NULL,
                          FALSE,
                          (G_PARAM_READABLE |
                           G_PARAM_STATIC_STRINGS));

  properties[PROP_CREATED_AT] =
    g_param_spec_boxed ("created-at", NULL, NULL,
                         G_TYPE_DATE_TIME,
                         (G_PARAM_READABLE |
                          G_PARAM_STATIC_STRINGS));

  properties[PROP_DESCRIPTION] =
    g_param_spec_string ("description", NULL, NULL,
                         NULL,
                         (G_PARAM_READABLE |
                          G_PARAM_STATIC_STRINGS));

  properties[PROP_DUE_DATE] =
    g_param_spec_boxed ("due-date", NULL, NULL,
                        G_TYPE_DATE_TIME,
                        (G_PARAM_READABLE |
                         G_PARAM_STATIC_STRINGS));

  properties[PROP_STATE] =
    g_param_spec_string ("state", NULL, NULL,
                         NULL,
                         (G_PARAM_READABLE |
                          G_PARAM_STATIC_STRINGS));

  properties[PROP_TITLE] =
    g_param_spec_string ("title", NULL, NULL,
                         NULL,
                         (G_PARAM_READABLE |
                          G_PARAM_STATIC_STRINGS));

  properties[PROP_UPDATED_AT] =
    g_param_spec_boxed ("updated-at", NULL, NULL,
                         G_TYPE_DATE_TIME,
                         (G_PARAM_READABLE |
                          G_PARAM_STATIC_STRINGS));

  properties[PROP_WEB_URL] =
    g_param_spec_string ("web-url", NULL, NULL,
                         NULL,
                         (G_PARAM_READABLE |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
gitlab_issue_init (GitlabIssue *self)
{
}

gint64
gitlab_issue_get_id (GitlabIssue *self)
{
  g_return_val_if_fail (GITLAB_IS_ISSUE (self), -1);

  return _gitlab_resource_get_int (GITLAB_RESOURCE (self), "id");
}

const char *
gitlab_issue_get_description (GitlabIssue *self)
{
  g_return_val_if_fail (GITLAB_IS_ISSUE (self), NULL);

  return _gitlab_resource_get_string (GITLAB_RESOURCE (self), "description");
}

const char *
gitlab_issue_get_state (GitlabIssue *self)
{
  g_return_val_if_fail (GITLAB_IS_ISSUE (self), NULL);

  return _gitlab_resource_get_string (GITLAB_RESOURCE (self), "state");
}

const char *
gitlab_issue_get_title (GitlabIssue *self)
{
  g_return_val_if_fail (GITLAB_IS_ISSUE (self), NULL);

  return _gitlab_resource_get_string (GITLAB_RESOURCE (self), "title");
}

const char *
gitlab_issue_get_web_url (GitlabIssue *self)
{
  g_return_val_if_fail (GITLAB_IS_ISSUE (self), NULL);

  return _gitlab_resource_get_string (GITLAB_RESOURCE (self), "web-url");
}

gboolean
gitlab_issue_get_confidential (GitlabIssue *self)
{
  g_return_val_if_fail (GITLAB_IS_ISSUE (self), FALSE);

  return _gitlab_resource_get_bool (GITLAB_RESOURCE (self), "confidential");
}

GDateTime *
gitlab_issue_dup_created_at (GitlabIssue *self)
{
  g_return_val_if_fail (GITLAB_IS_ISSUE (self), NULL);

  return _gitlab_resource_dup_date_time (GITLAB_RESOURCE (self), "created_at");
}

GDateTime *
gitlab_issue_dup_closed_at (GitlabIssue *self)
{
  g_return_val_if_fail (GITLAB_IS_ISSUE (self), NULL);

  return _gitlab_resource_dup_date_time (GITLAB_RESOURCE (self), "closed_at");
}

GDateTime *
gitlab_issue_dup_due_date (GitlabIssue *self)
{
  g_return_val_if_fail (GITLAB_IS_ISSUE (self), NULL);

  return _gitlab_resource_dup_date_time (GITLAB_RESOURCE (self), "due_date");
}

GDateTime *
gitlab_issue_dup_updated_at (GitlabIssue *self)
{
  g_return_val_if_fail (GITLAB_IS_ISSUE (self), NULL);

  return _gitlab_resource_dup_date_time (GITLAB_RESOURCE (self), "updated_at");
}
