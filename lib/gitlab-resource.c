/* gitlab-resource.c
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "gitlab-resource-private.h"

G_DEFINE_ABSTRACT_TYPE (GitlabResource, gitlab_resource, G_TYPE_OBJECT)

static void
gitlab_resource_real_set_json (GitlabResource *self,
                               JsonNode       *node)
{
}

static void
gitlab_resource_class_init (GitlabResourceClass *klass)
{
  klass->set_json = gitlab_resource_real_set_json;
}

static void
gitlab_resource_init (GitlabResource *self)
{
}

void
_gitlab_resource_set_json (GitlabResource *self,
                           JsonNode       *node)
{
  g_return_if_fail (GITLAB_IS_RESOURCE (self));
  g_return_if_fail (node != NULL);

  if (self->node == node)
    return;

  g_clear_pointer (&self->node, json_node_unref);
  self->node = json_node_ref (node);

  GITLAB_RESOURCE_GET_CLASS (self)->set_json (self, node);
}

gint64
_gitlab_resource_get_int (GitlabResource *self,
                          const char     *key)
{
  g_return_val_if_fail (GITLAB_IS_RESOURCE (self), 0);
  g_return_val_if_fail (key != NULL, 0);

  if (self->node == NULL || !JSON_NODE_HOLDS_OBJECT (self->node))
    return 0;

  return json_object_get_int_member (json_node_get_object (self->node), key);
}

const char *
_gitlab_resource_get_string (GitlabResource *self,
                             const char     *key)
{
  g_return_val_if_fail (GITLAB_IS_RESOURCE (self), NULL);
  g_return_val_if_fail (key != NULL, NULL);

  if (self->node == NULL || !JSON_NODE_HOLDS_OBJECT (self->node))
    return NULL;

  return json_object_get_string_member (json_node_get_object (self->node), key);
}

GDateTime *
_gitlab_resource_dup_date_time (GitlabResource *self,
                                const char     *key)
{
  JsonObject *obj;
  const char *str;

  g_return_val_if_fail (GITLAB_IS_RESOURCE (self), NULL);
  g_return_val_if_fail (key != NULL, NULL);

  if (self->node == NULL || !JSON_NODE_HOLDS_OBJECT (self->node))
    return NULL;

  obj = json_node_get_object (self->node);
  if (!json_object_has_member (obj, key))
    return NULL;

  if (!(str = json_object_get_string_member (obj, key)))
    return NULL;

  return g_date_time_new_from_iso8601 (str, NULL);
}

gboolean
_gitlab_resource_get_bool (GitlabResource *self,
                           const char     *key)
{
  g_return_val_if_fail (GITLAB_IS_RESOURCE (self), FALSE);
  g_return_val_if_fail (key != NULL, FALSE);

  if (self->node == NULL || !JSON_NODE_HOLDS_OBJECT (self->node))
    return FALSE;

  return json_object_get_boolean_member (json_node_get_object (self->node), key);
}
