/* gitlab-project.c
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "gitlab-project.h"
#include "gitlab-resource-private.h"

struct _GitlabProject
{
  GitlabResource parent_instance;
};

struct _GitlabProjectClass
{
  GitlabResourceClass parent_class;
};

enum {
  PROP_0,
  PROP_DESCRIPTION,
  PROP_ID,
  PROP_NAME,
  PROP_WEB_URL,
  N_PROPS
};

G_DEFINE_FINAL_TYPE (GitlabProject, gitlab_project, GITLAB_TYPE_RESOURCE)

static GParamSpec *properties[N_PROPS];

static void
gitlab_project_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  GitlabProject *self = GITLAB_PROJECT (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_int64 (value, gitlab_project_get_id (self));
      break;

    case PROP_DESCRIPTION:
      g_value_set_string (value, gitlab_project_get_description (self));
      break;

    case PROP_NAME:
      g_value_set_string (value, gitlab_project_get_name (self));
      break;

    case PROP_WEB_URL:
      g_value_set_string (value, gitlab_project_get_web_url (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_project_class_init (GitlabProjectClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = gitlab_project_get_property;

  properties[PROP_ID] =
    g_param_spec_int64 ("id", NULL, NULL,
                        0, G_MAXINT64, 0,
                        (G_PARAM_READABLE |
                         G_PARAM_STATIC_STRINGS));

  properties[PROP_DESCRIPTION] =
    g_param_spec_string ("description", NULL, NULL,
                         NULL,
                         (G_PARAM_READABLE |
                          G_PARAM_STATIC_STRINGS));

  properties[PROP_NAME] =
    g_param_spec_string ("name", NULL, NULL,
                         NULL,
                         (G_PARAM_READABLE |
                          G_PARAM_STATIC_STRINGS));

  properties[PROP_WEB_URL] =
    g_param_spec_string ("web-url", NULL, NULL,
                         NULL,
                         (G_PARAM_READABLE |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
gitlab_project_init (GitlabProject *self)
{
}

gint64
gitlab_project_get_id (GitlabProject *self)
{
  g_return_val_if_fail (GITLAB_IS_PROJECT (self), -1);

  return _gitlab_resource_get_int (GITLAB_RESOURCE (self), "id");
}

const char *
gitlab_project_get_description (GitlabProject *self)
{
  g_return_val_if_fail (GITLAB_IS_PROJECT (self), NULL);

  return _gitlab_resource_get_string (GITLAB_RESOURCE (self), "description");
}

const char *
gitlab_project_get_name (GitlabProject *self)
{
  g_return_val_if_fail (GITLAB_IS_PROJECT (self), NULL);

  return _gitlab_resource_get_string (GITLAB_RESOURCE (self), "name");
}

const char *
gitlab_project_get_web_url (GitlabProject *self)
{
  g_return_val_if_fail (GITLAB_IS_PROJECT (self), NULL);

  return _gitlab_resource_get_string (GITLAB_RESOURCE (self), "web-url");
}
