/* gitlab-issue-filter.c
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "gitlab-filter-private.h"
#include "gitlab-issue-filter.h"
#include "gitlab-utils-private.h"

struct _GitlabIssueFilter
{
  GitlabFilter parent_instance;
  gint64 assignee_id;
  gint64 author_id;
};

struct _GitlabIssueFilterClass
{
  GitlabFilterClass parent_class;
};

enum {
  PROP_0,
  PROP_ASSIGNEE_ID,
  PROP_AUTHOR_ID,
  N_PROPS
};

G_DEFINE_FINAL_TYPE (GitlabIssueFilter, gitlab_issue_filter, GITLAB_TYPE_FILTER)

static GParamSpec *properties[N_PROPS];

static char **
gitlab_issue_filter_to_params (GitlabFilter *filter)
{
  GitlabIssueFilter *self = (GitlabIssueFilter *)filter;
  char **params = NULL;

  g_assert (GITLAB_IS_ISSUE_FILTER (self));

  if (self->assignee_id)
    params = _gitlab_params_set_int (params, "assignee_id", self->assignee_id);

  if (self->author_id)
    params = _gitlab_params_set_int (params, "author_id", self->author_id);

  return params;
}

static void
gitlab_issue_filter_dispose (GObject *object)
{
  G_OBJECT_CLASS (gitlab_issue_filter_parent_class)->dispose (object);
}

static void
gitlab_issue_filter_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  GitlabIssueFilter *self = GITLAB_ISSUE_FILTER (object);

  switch (prop_id)
    {
    case PROP_ASSIGNEE_ID:
      g_value_set_int64 (value, gitlab_issue_filter_get_assignee_id (self));
      break;

    case PROP_AUTHOR_ID:
      g_value_set_int64 (value, gitlab_issue_filter_get_assignee_id (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_issue_filter_set_property (GObject      *object,
                                  guint         prop_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  GitlabIssueFilter *self = GITLAB_ISSUE_FILTER (object);

  switch (prop_id)
    {
    case PROP_ASSIGNEE_ID:
      gitlab_issue_filter_set_assignee_id (self, g_value_get_int64 (value));
      break;

    case PROP_AUTHOR_ID:
      gitlab_issue_filter_set_author_id (self, g_value_get_int64 (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_issue_filter_class_init (GitlabIssueFilterClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GitlabFilterClass *filter_class = GITLAB_FILTER_CLASS (klass);

  object_class->dispose = gitlab_issue_filter_dispose;
  object_class->get_property = gitlab_issue_filter_get_property;
  object_class->set_property = gitlab_issue_filter_set_property;

  filter_class->to_params = gitlab_issue_filter_to_params;

  properties[PROP_ASSIGNEE_ID] =
    g_param_spec_int64 ("assignee-id", NULL, NULL,
                        -1, G_MAXINT64, -1,
                        (G_PARAM_READWRITE |
                         G_PARAM_EXPLICIT_NOTIFY |
                         G_PARAM_STATIC_STRINGS));

  properties[PROP_AUTHOR_ID] =
    g_param_spec_int64 ("author-id", NULL, NULL,
                        -1, G_MAXINT64, -1,
                        (G_PARAM_READWRITE |
                         G_PARAM_EXPLICIT_NOTIFY |
                         G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
gitlab_issue_filter_init (GitlabIssueFilter *self)
{
  self->assignee_id = -1;
  self->author_id = -1;
}

GitlabIssueFilter *
gitlab_issue_filter_new (void)
{
  return g_object_new (GITLAB_TYPE_ISSUE_FILTER, NULL);
}

gint64
gitlab_issue_filter_get_assignee_id (GitlabIssueFilter *self)
{
  g_return_val_if_fail (GITLAB_IS_ISSUE_FILTER (self), -1);

  return self->assignee_id;
}

void
gitlab_issue_filter_set_assignee_id (GitlabIssueFilter *self,
                                     gint64             assignee_id)
{
  g_return_if_fail (GITLAB_IS_ISSUE_FILTER (self));

  if (_g_set_int64 (&self->assignee_id, assignee_id))
    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ASSIGNEE_ID]);
}

gint64
gitlab_issue_filter_get_author_id (GitlabIssueFilter *self)
{
  g_return_val_if_fail (GITLAB_IS_ISSUE_FILTER (self), -1);

  return self->author_id;
}

void
gitlab_issue_filter_set_author_id (GitlabIssueFilter *self,
                                   gint64             author_id)
{
  g_return_if_fail (GITLAB_IS_ISSUE_FILTER (self));

  if (_g_set_int64 (&self->author_id, author_id))
    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_AUTHOR_ID]);
}
