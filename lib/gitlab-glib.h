/* gitlab-glib.h
 *
 * Copyright 2024 Christian Hergert
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <gio/gio.h>
#include <libdex.h>

G_BEGIN_DECLS

#define GITLAB_INSIDE
# include "gitlab-api-key.h"
# include "gitlab-client.h"
# include "gitlab-filter.h"
# include "gitlab-issue.h"
# include "gitlab-issue-filter.h"
# include "gitlab-issues.h"
# include "gitlab-list-item.h"
# include "gitlab-list-model.h"
# include "gitlab-project.h"
# include "gitlab-project-filter.h"
# include "gitlab-projects.h"
# include "gitlab-resource.h"
# include "gitlab-secret.h"
# include "gitlab-service.h"
# include "gitlab-version.h"
# include "gitlab-version-macros.h"
#undef GITLAB_INSIDE

G_END_DECLS
