/* gitlab-issue-filter.h
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined (GITLAB_INSIDE) && !defined (GITLAB_COMPILATION)
# error "Only <gitlab-glib.h> can be included directly."
#endif

#include "gitlab-types.h"

G_BEGIN_DECLS

#define GITLAB_TYPE_ISSUE_FILTER            (gitlab_issue_filter_get_type())
#define GITLAB_ISSUE_FILTER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GITLAB_TYPE_ISSUE_FILTER, GitlabIssueFilter))
#define GITLAB_ISSUE_FILTER_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GITLAB_TYPE_ISSUE_FILTER, GitlabIssueFilter const))
#define GITLAB_ISSUE_FILTER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GITLAB_TYPE_ISSUE_FILTER, GitlabIssueFilterClass))
#define GITLAB_IS_ISSUE_FILTER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GITLAB_TYPE_ISSUE_FILTER))
#define GITLAB_IS_ISSUE_FILTER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GITLAB_TYPE_ISSUE_FILTER))
#define GITLAB_ISSUE_FILTER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  GITLAB_TYPE_ISSUE_FILTER, GitlabIssueFilterClass))

GITLAB_AVAILABLE_IN_ALL
GType              gitlab_issue_filter_get_type        (void) G_GNUC_CONST;
GITLAB_AVAILABLE_IN_ALL
GitlabIssueFilter *gitlab_issue_filter_new             (void);
GITLAB_AVAILABLE_IN_ALL
gint64             gitlab_issue_filter_get_assignee_id (GitlabIssueFilter *self);
GITLAB_AVAILABLE_IN_ALL
void               gitlab_issue_filter_set_assignee_id (GitlabIssueFilter *self,
                                                        gint64             assignee_id);
GITLAB_AVAILABLE_IN_ALL
gint64             gitlab_issue_filter_get_author_id   (GitlabIssueFilter *self);
GITLAB_AVAILABLE_IN_ALL
void               gitlab_issue_filter_set_author_id   (GitlabIssueFilter *self,
                                                        gint64             author_id);

G_END_DECLS
