/* gitlab-issues.c
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "gitlab-filter-private.h"
#include "gitlab-issue.h"
#include "gitlab-issue-filter.h"
#include "gitlab-issues.h"
#include "gitlab-service-private.h"
#include "gitlab-wrapped-list-model-private.h"

struct _GitlabIssues
{
  GitlabService parent_instance;
};

struct _GitlabIssuesClass
{
  GitlabServiceClass parent_class;
};

G_DEFINE_FINAL_TYPE (GitlabIssues, gitlab_issues, GITLAB_TYPE_SERVICE)

static void
gitlab_issues_class_init (GitlabIssuesClass *klass)
{
}

static void
gitlab_issues_init (GitlabIssues *self)
{
}

GitlabIssues *
gitlab_issues_new (GitlabClient *client)
{
  g_return_val_if_fail (GITLAB_IS_CLIENT (client), NULL);

  return g_object_new (GITLAB_TYPE_ISSUES,
                       "client", client,
                       NULL);
}

/**
 * gitlab_issues_list_for_project:
 * @self: a #GitlabIssues
 * @project_id: the project id of the project to filter
 * @filter: (nullable): optional search filters
 *
 * Returns: (transfer full): a new #GitlabListModel of issues
 */
GitlabListModel *
gitlab_issues_list_for_project (GitlabIssues      *self,
                                gint64             project_id,
                                GitlabIssueFilter *filter)
{
  g_autofree char *path = NULL;
  g_auto(GStrv) params = NULL;
  GitlabClient *client;

  g_return_val_if_fail (GITLAB_IS_ISSUES (self), NULL);
  g_return_val_if_fail (project_id > 0, NULL);
  g_return_val_if_fail (!filter || GITLAB_IS_ISSUE_FILTER (filter), NULL);

  client = gitlab_service_get_client (GITLAB_SERVICE (self));
  path = g_strdup_printf ("/api/v4/projects/%"G_GINT64_FORMAT"/issues", project_id);

  if (filter != NULL)
    params = _gitlab_filter_to_params (GITLAB_FILTER (filter));

  return gitlab_wrapped_list_model_new (GITLAB_TYPE_ISSUE, client, path, (const char * const *)params);
}

/**
 * gitlab_issues_list_for_project_name:
 * @self: a #GitlabIssues
 * @project_name: the name of the project to filter
 * @filter: (nullable): optional search filters
 *
 * Returns: (transfer full): a new #GitlabListModel of issues
 */
GitlabListModel *
gitlab_issues_list_for_project_name (GitlabIssues      *self,
                                     const char        *project_name,
                                     GitlabIssueFilter *filter)
{
  g_autofree char *path = NULL;
  g_autoptr(GString) encoded = NULL;
  g_auto(GStrv) params = NULL;
  GitlabClient *client;

  g_return_val_if_fail (GITLAB_IS_ISSUES (self), NULL);
  g_return_val_if_fail (project_name != NULL, NULL);
  g_return_val_if_fail (!filter || GITLAB_IS_ISSUE_FILTER (filter), NULL);

  encoded = g_string_new (project_name);
  g_string_replace (encoded, "/", "%2F", 0);

  client = gitlab_service_get_client (GITLAB_SERVICE (self));
  path = g_strdup_printf ("/api/v4/projects/%s/issues", encoded->str);

  if (filter != NULL)
    params = _gitlab_filter_to_params (GITLAB_FILTER (filter));

  return gitlab_wrapped_list_model_new (GITLAB_TYPE_ISSUE, client, path, (const char * const *)params);
}
