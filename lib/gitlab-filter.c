/* gitlab-filter.c
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "gitlab-filter-private.h"

G_DEFINE_ABSTRACT_TYPE (GitlabFilter, gitlab_filter, G_TYPE_OBJECT)

static void
gitlab_filter_class_init (GitlabFilterClass *klass)
{
}

static void
gitlab_filter_init (GitlabFilter *self)
{
}

char **
_gitlab_filter_to_params (GitlabFilter *self)
{
  g_return_val_if_fail (GITLAB_IS_FILTER (self), NULL);

  return GITLAB_FILTER_GET_CLASS (self)->to_params (self);
}
