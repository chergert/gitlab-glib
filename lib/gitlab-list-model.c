/* gitlab-list-model.c
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "gitlab-list-item.h"
#include "gitlab-list-model-private.h"
#include "gitlab-resource.h"

enum {
  PROP_0,
  PROP_RESOURCE_TYPE,
  PROP_N_ITEMS,
  N_PROPS
};

static GType
gitlab_list_model_get_item_type (GListModel *model)
{
  return GITLAB_TYPE_LIST_ITEM;
}

static guint
gitlab_list_model_get_n_items (GListModel *model)
{
  return GITLAB_LIST_MODEL_GET_CLASS (model)->get_n_items (GITLAB_LIST_MODEL (model));
}

static gpointer
gitlab_list_model_get_item (GListModel *model,
                            guint       position)
{
  return GITLAB_LIST_MODEL_GET_CLASS (model)->get_item (GITLAB_LIST_MODEL (model), position);
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  iface->get_item_type = gitlab_list_model_get_item_type;
  iface->get_n_items = gitlab_list_model_get_n_items;
  iface->get_item = gitlab_list_model_get_item;
}

G_DEFINE_ABSTRACT_TYPE_WITH_CODE (GitlabListModel, gitlab_list_model, G_TYPE_OBJECT,
                                  G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_model_iface_init))

static GParamSpec *properties[N_PROPS];

static void
gitlab_list_model_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  GitlabListModel *self = GITLAB_LIST_MODEL (object);

  switch (prop_id)
    {
    case PROP_N_ITEMS:
      g_value_set_uint (value, g_list_model_get_n_items (G_LIST_MODEL (self)));
      break;

    case PROP_RESOURCE_TYPE:
      g_value_set_gtype (value, self->resource_type);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_list_model_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  GitlabListModel *self = GITLAB_LIST_MODEL (object);

  switch (prop_id)
    {
    case PROP_RESOURCE_TYPE:
      self->resource_type = g_value_get_gtype (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_list_model_class_init (GitlabListModelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = gitlab_list_model_get_property;
  object_class->set_property = gitlab_list_model_set_property;

  properties[PROP_RESOURCE_TYPE] =
    g_param_spec_gtype ("resource-type", NULL, NULL,
                        GITLAB_TYPE_RESOURCE,
                        (G_PARAM_READWRITE |
                         G_PARAM_CONSTRUCT_ONLY |
                         G_PARAM_STATIC_STRINGS));

  properties[PROP_N_ITEMS] =
    g_param_spec_uint ("n-items", NULL, NULL,
                       0, G_MAXUINT-1, 0,
                       (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
gitlab_list_model_init (GitlabListModel *self)
{
  self->resource_type = GITLAB_TYPE_RESOURCE;
}

/**
 * gitlab_list_model_populate_all:
 * @self: a #GitlabListModel
 *
 * Gets a #DexFuture that will resolve when all of the items within
 * the model have been loaded from the API client.
 *
 * Upon failure, the future will reject with error.
 *
 * Returns: (transfer full): a #DexFuture
 */
DexFuture *
gitlab_list_model_populate_all (GitlabListModel *self)
{
  DexFuture *ret;

  g_return_val_if_fail (GITLAB_IS_LIST_MODEL (self), NULL);

  ret = GITLAB_LIST_MODEL_GET_CLASS (self)->populate_all (self);

  g_return_val_if_fail (DEX_IS_FUTURE (ret), NULL);

  return ret;
}

void
_gitlab_list_model_notify_n_items (GitlabListModel *self)
{
  g_return_if_fail (GITLAB_IS_LIST_MODEL (self));

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_N_ITEMS]);
}
