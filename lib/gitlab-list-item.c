/* gitlab-list-item.c
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "gitlab-list-item-private.h"

struct _GitlabListItem
{
  GObject parent_instance;
  gpointer item;
};

enum {
  PROP_0,
  PROP_ITEM,
  N_PROPS
};

G_DEFINE_FINAL_TYPE (GitlabListItem, gitlab_list_item, G_TYPE_OBJECT)

static GParamSpec *properties[N_PROPS];

static void
gitlab_list_item_dispose (GObject *object)
{
  GitlabListItem *self = (GitlabListItem *)object;

  g_clear_object (&self->item);

  G_OBJECT_CLASS (gitlab_list_item_parent_class)->dispose (object);
}

static void
gitlab_list_item_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  GitlabListItem *self = GITLAB_LIST_ITEM (object);

  switch (prop_id)
    {
    case PROP_ITEM:
      g_value_set_object (value, gitlab_list_item_get_item (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_list_item_class_init (GitlabListItemClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = gitlab_list_item_dispose;
  object_class->get_property = gitlab_list_item_get_property;

  properties[PROP_ITEM] =
    g_param_spec_object ("item", NULL, NULL,
                         G_TYPE_OBJECT,
                         (G_PARAM_READABLE |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
gitlab_list_item_init (GitlabListItem *self)
{
}

/**
 * gitlab_list_item_get_item:
 * @self: a #GitlabListItem
 *
 * Gets the item for the #GitlabListItem
 *
 * Returns: (transfer none) (nullable): a #GObject or %NULL
 */
gpointer
gitlab_list_item_get_item (GitlabListItem *self)
{
  g_return_val_if_fail (GITLAB_IS_LIST_ITEM (self), NULL);

  return self->item;
}

void
_gitlab_list_item_set_item (GitlabListItem *self,
                            gpointer        item)
{
  g_return_if_fail (GITLAB_IS_LIST_ITEM (self));
  g_return_if_fail (!item || G_IS_OBJECT (item));

  if (g_set_object (&self->item, item))
    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ITEM]);
}
