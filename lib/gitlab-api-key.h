/* gitlab-api-key.h
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#if !defined (GITLAB_INSIDE) && !defined (GITLAB_COMPILATION)
# error "Only <gitlab-glib.h> can be included directly."
#endif

#include "gitlab-types.h"

G_BEGIN_DECLS

#define GITLAB_TYPE_API_KEY            (gitlab_api_key_get_type())
#define GITLAB_API_KEY(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GITLAB_TYPE_API_KEY, GitlabApiKey))
#define GITLAB_API_KEY_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GITLAB_TYPE_API_KEY, GitlabApiKey const))
#define GITLAB_API_KEY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GITLAB_TYPE_API_KEY, GitlabApiKeyClass))
#define GITLAB_IS_API_KEY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GITLAB_TYPE_API_KEY))
#define GITLAB_IS_API_KEY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GITLAB_TYPE_API_KEY))

GITLAB_AVAILABLE_IN_ALL
GType         gitlab_api_key_get_type  (void) G_GNUC_CONST;
GITLAB_AVAILABLE_IN_ALL
GitlabApiKey *gitlab_api_key_new       (const char   *api_key);
GITLAB_AVAILABLE_IN_ALL
char         *gitlab_api_key_to_string (GitlabApiKey *self);

G_END_DECLS
