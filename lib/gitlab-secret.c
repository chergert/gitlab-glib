/* gitlab-secret.c
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "gitlab-secret-private.h"

G_DEFINE_ABSTRACT_TYPE (GitlabSecret, gitlab_secret, G_TYPE_OBJECT)

static void
gitlab_secret_class_init (GitlabSecretClass *klass)
{
}

static void
gitlab_secret_init (GitlabSecret *self)
{
}

void
_gitlab_secret_sign (GitlabSecret *self,
                     SoupMessage  *message)
{
  g_return_if_fail (GITLAB_IS_SECRET (self));
  g_return_if_fail (SOUP_IS_MESSAGE (message));

  GITLAB_SECRET_GET_CLASS (self)->sign (self, message);
}
