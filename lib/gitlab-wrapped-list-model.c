/* gitlab-wrapped-list-model.c
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "gitlab-client.h"
#include "gitlab-list-item.h"
#include "gitlab-list-model-private.h"
#include "gitlab-offset-list-model-private.h"
#include "gitlab-page-list-model-private.h"
#include "gitlab-progressive-list-model-private.h"
#include "gitlab-resource.h"
#include "gitlab-wrapped-list-model-private.h"
#include "gitlab-utils-private.h"

struct _GitlabWrappedListModel
{
  GitlabListModel   parent_instance;
  GitlabClient     *client;
  char             *path;
  char            **params;
  GitlabListModel  *wrapped;
  DexFuture        *init;
  guint             page_size;
};

struct _GitlabWrappedListModelClass
{
  GitlabListModelClass parent_class;
};

enum {
  PROP_0,
  PROP_CLIENT,
  PROP_PAGE_SIZE,
  PROP_PARAMS,
  PROP_PATH,
  N_PROPS
};

static guint
gitlab_wrapped_list_model_get_n_items (GListModel *model)
{
  GitlabWrappedListModel *self = GITLAB_WRAPPED_LIST_MODEL (model);

  if (self->wrapped == NULL)
    return 0;

  return g_list_model_get_n_items (G_LIST_MODEL (self->wrapped));
}

static gpointer
gitlab_wrapped_list_model_get_item (GListModel *model,
                                    guint       position)
{
  GitlabWrappedListModel *self = GITLAB_WRAPPED_LIST_MODEL (model);

  if (self->wrapped == NULL)
    return NULL;

  return g_list_model_get_item (G_LIST_MODEL (self->wrapped), position);
}

static GType
gitlab_wrapped_list_model_get_item_type (GListModel *model)
{
  return GITLAB_TYPE_LIST_ITEM;
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  iface->get_n_items = gitlab_wrapped_list_model_get_n_items;
  iface->get_item = gitlab_wrapped_list_model_get_item;
  iface->get_item_type = gitlab_wrapped_list_model_get_item_type;
}

static void
gitlab_wrapped_list_model_items_changed_cb (GitlabWrappedListModel *self,
                                            guint                   position,
                                            guint                   removed,
                                            guint                   added,
                                            GListModel             *wrapped)
{
  if (removed != added)
    _gitlab_list_model_notify_n_items (GITLAB_LIST_MODEL (self));
}

static DexFuture *
gitlab_wrapped_list_model_init_fiber (gpointer user_data)
{
  GitlabWrappedListModel *self = user_data;
  g_autoptr(GitlabPageListModel) first_page = NULL;
  g_autoptr(GError) error = NULL;
  g_auto(GStrv) params = NULL;
  guint x_total;

  g_assert (GITLAB_IS_WRAPPED_LIST_MODEL (self));

  /* We need get the first page of results and see if the response includes
   * the x-total header. If so, we can use the GitlabOffsetListModel to provide
   * a listmodel which has an appropriate number of items from the start.
   *
   * Otherwise, we will use GitlabProgressiveListModel which will append pages
   * as each final page is accessed the first time.
   */

  params = g_strdupv (self->params);
  params = _gitlab_params_set (params, "page", "1");
  params = _gitlab_params_set_formatted (params, "per_page", "%u", self->page_size);

  first_page = gitlab_page_list_model_new (GITLAB_LIST_MODEL (self)->resource_type,
                                           self->client,
                                           self->path,
                                           (const char * const *)params,
                                           0);
  if (!dex_await (gitlab_page_list_model_await (first_page), &error))
    return dex_future_new_for_error (g_steal_pointer (&error));

  if (gitlab_page_list_model_get_x_total (first_page, &x_total))
    self->wrapped = gitlab_offset_list_model_new (GITLAB_LIST_MODEL (self)->resource_type,
                                                  self->client,
                                                  self->path,
                                                  (const char * const *)self->params,
                                                  first_page,
                                                  self->page_size,
                                                  x_total);
  else
    self->wrapped = gitlab_progressive_list_model_new (GITLAB_LIST_MODEL (self)->resource_type,
                                                       self->client,
                                                       self->path,
                                                       (const char * const *)self->params,
                                                       first_page);

  g_signal_connect_object (self->wrapped,
                           "items-changed",
                           G_CALLBACK (gitlab_wrapped_list_model_items_changed_cb),
                           self,
                           G_CONNECT_SWAPPED);

  return dex_future_new_for_boolean (TRUE);
}

static void
gitlab_wrapped_list_model_init_async (GAsyncInitable      *initable,
                                      int                  io_priority,
                                      GCancellable        *cancellable,
                                      GAsyncReadyCallback  callback,
                                      gpointer             user_data)
{
  GitlabWrappedListModel *self = (GitlabWrappedListModel *)initable;
  g_autoptr(DexAsyncResult) result = NULL;

  g_assert (GITLAB_IS_WRAPPED_LIST_MODEL (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  if (self->init == NULL)
    self->init = dex_scheduler_spawn (NULL, 0,
                                      gitlab_wrapped_list_model_init_fiber,
                                      g_object_ref (self),
                                      g_object_unref);

  result = dex_async_result_new (self, cancellable, callback, user_data);
  dex_async_result_await (result, dex_ref (self->init));
}

static gboolean
gitlab_wrapped_list_model_init_finish (GAsyncInitable  *initable,
                                       GAsyncResult    *result,
                                       GError         **error)
{
  return dex_async_result_propagate_boolean (DEX_ASYNC_RESULT (result), error);
}

static void
async_initable_iface_init (GAsyncInitableIface *iface)
{
  iface->init_async = gitlab_wrapped_list_model_init_async;
  iface->init_finish = gitlab_wrapped_list_model_init_finish;
}

G_DEFINE_FINAL_TYPE_WITH_CODE (GitlabWrappedListModel, gitlab_wrapped_list_model, GITLAB_TYPE_LIST_MODEL,
                               G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE, async_initable_iface_init)
                               G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_model_iface_init))

static GParamSpec *properties[N_PROPS];

static DexFuture *
gitlab_wrapped_list_model_populate_all_cb (DexFuture *completed,
                                           gpointer   user_data)
{
  GitlabWrappedListModel *self = user_data;

  g_assert (DEX_IS_FUTURE (completed));
  g_assert (GITLAB_IS_WRAPPED_LIST_MODEL (self));

  return dex_future_new_take_object (g_object_ref (self));
}

static DexFuture *
gitlab_wrapped_list_model_populate_all_wrapped (DexFuture *completed,
                                                gpointer   user_data)
{
  GitlabWrappedListModel *self = user_data;
  DexFuture *future;

  g_assert (DEX_IS_FUTURE (completed));
  g_assert (GITLAB_IS_WRAPPED_LIST_MODEL (self));

  if (self->wrapped != NULL)
    future = gitlab_list_model_populate_all (GITLAB_LIST_MODEL (self->wrapped));
  else
    future = dex_future_new_for_boolean (TRUE);

  return dex_future_then (future,
                          gitlab_wrapped_list_model_populate_all_cb,
                          g_object_ref (self),
                          g_object_unref);
}

static DexFuture *
gitlab_wrapped_list_model_populate_all (GitlabListModel *list_model)
{
  GitlabWrappedListModel *self = GITLAB_WRAPPED_LIST_MODEL (list_model);

  g_assert (GITLAB_IS_WRAPPED_LIST_MODEL (self));

  if (self->init == NULL)
    g_async_initable_init_async (G_ASYNC_INITABLE (self), 0, NULL, NULL, NULL);

  return dex_future_then (dex_ref (self->init),
                          gitlab_wrapped_list_model_populate_all_wrapped,
                          g_object_ref (self),
                          g_object_unref);
}

static void
gitlab_wrapped_list_model_dispose (GObject *object)
{
  GitlabWrappedListModel *self = (GitlabWrappedListModel *)object;

  g_clear_object (&self->client);
  g_clear_object (&self->wrapped);
  g_clear_pointer (&self->path, g_free);
  g_clear_pointer (&self->params, g_strfreev);
  dex_clear (&self->init);

  G_OBJECT_CLASS (gitlab_wrapped_list_model_parent_class)->dispose (object);
}

static void
gitlab_wrapped_list_model_get_property (GObject    *object,
                                        guint       prop_id,
                                        GValue     *value,
                                        GParamSpec *pspec)
{
  GitlabWrappedListModel *self = GITLAB_WRAPPED_LIST_MODEL (object);

  switch (prop_id)
    {
    case PROP_CLIENT:
      g_value_set_object (value, self->client);
      break;

    case PROP_PATH:
      g_value_set_string (value, self->path);
      break;

    case PROP_PARAMS:
      g_value_set_boxed (value, self->params);
      break;

    case PROP_PAGE_SIZE:
      g_value_set_uint (value, self->page_size);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_wrapped_list_model_set_property (GObject      *object,
                                        guint         prop_id,
                                        const GValue *value,
                                        GParamSpec   *pspec)
{
  GitlabWrappedListModel *self = GITLAB_WRAPPED_LIST_MODEL (object);

  switch (prop_id)
    {
    case PROP_CLIENT:
      self->client = g_value_dup_object (value);
      break;

    case PROP_PATH:
      self->path = g_value_dup_string (value);
      break;

    case PROP_PAGE_SIZE:
      self->page_size = g_value_get_uint (value);
      break;

    case PROP_PARAMS:
      self->params = g_value_dup_boxed (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_wrapped_list_model_class_init (GitlabWrappedListModelClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GitlabListModelClass *list_model_class = GITLAB_LIST_MODEL_CLASS (klass);

  object_class->dispose = gitlab_wrapped_list_model_dispose;
  object_class->get_property = gitlab_wrapped_list_model_get_property;
  object_class->set_property = gitlab_wrapped_list_model_set_property;

  list_model_class->populate_all = gitlab_wrapped_list_model_populate_all;

  properties[PROP_CLIENT] =
    g_param_spec_object ("client", NULL, NULL,
                         GITLAB_TYPE_CLIENT,
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_STATIC_STRINGS));

  properties[PROP_PAGE_SIZE] =
    g_param_spec_uint ("page-size", NULL, NULL,
                       1, 20, 20,
                       (G_PARAM_READWRITE |
                        G_PARAM_CONSTRUCT_ONLY |
                        G_PARAM_STATIC_STRINGS));

  properties[PROP_PARAMS] =
    g_param_spec_boxed ("params", NULL, NULL,
                        G_TYPE_STRV,
                        (G_PARAM_READWRITE |
                         G_PARAM_CONSTRUCT_ONLY |
                         G_PARAM_STATIC_STRINGS));

  properties[PROP_PATH] =
    g_param_spec_string ("path", NULL, NULL,
                         NULL,
                         (G_PARAM_READWRITE |
                          G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
gitlab_wrapped_list_model_init (GitlabWrappedListModel *self)
{
  self->page_size = 20;
}

GitlabListModel *
gitlab_wrapped_list_model_new (GType               resource_type,
                               GitlabClient       *client,
                               const char         *path,
                               const char * const *params)
{
  GitlabWrappedListModel *self;

  g_return_val_if_fail (g_type_is_a (resource_type, GITLAB_TYPE_RESOURCE), NULL);
  g_return_val_if_fail (resource_type != GITLAB_TYPE_RESOURCE, NULL);
  g_return_val_if_fail (GITLAB_IS_CLIENT (client), NULL);
  g_return_val_if_fail (path != NULL, NULL);

  self = g_object_new (GITLAB_TYPE_WRAPPED_LIST_MODEL,
                       "client", client,
                       "params", params,
                       "path", path,
                       "resource-type", resource_type,
                       NULL);

  g_async_initable_init_async (G_ASYNC_INITABLE (self), 0, NULL, NULL, NULL);

  return GITLAB_LIST_MODEL (self);
}
