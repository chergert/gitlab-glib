/*
 * gitlab-json-private.h
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <json-glib/json-glib.h>
#include <libdex.h>

G_BEGIN_DECLS

static inline void
gitlab_json_parse_stream_cb (GObject      *object,
                             GAsyncResult *result,
                             gpointer      user_data)
{
  g_autoptr(DexPromise) promise = user_data;
  GError *error = NULL;

  g_assert (JSON_IS_PARSER (object));
  g_assert (G_IS_ASYNC_RESULT (result));
  g_assert (DEX_IS_PROMISE (promise));

  if (json_parser_load_from_stream_finish (JSON_PARSER (object), result, &error))
    dex_promise_resolve_object (promise, g_object_ref (object));
  else
    dex_promise_reject (promise, error);
}

static inline DexFuture *
gitlab_json_parse_stream (GInputStream *stream)
{
  g_autoptr(JsonParser) parser = NULL;
  DexPromise *promise;

  g_return_val_if_fail (G_IS_INPUT_STREAM (stream), NULL);

  promise = dex_promise_new_cancellable ();
  parser = json_parser_new ();

  json_parser_load_from_stream_async (parser,
                                      stream,
                                      dex_promise_get_cancellable (promise),
                                      gitlab_json_parse_stream_cb,
                                      dex_ref (promise));

  return DEX_FUTURE (promise);
}


static inline DexFuture *
gitlab_json_parse_bytes_fiber (gpointer user_data)
{
  g_autoptr(JsonParser) parser = json_parser_new ();
  g_autoptr(GError) error = NULL;
  GBytes *bytes = user_data;

  if (!json_parser_load_from_data (parser,
                                   g_bytes_get_data (bytes, NULL),
                                   g_bytes_get_size (bytes),
                                   &error))
    return dex_future_new_for_error (g_steal_pointer (&error));
  else
    return dex_future_new_take_object (g_steal_pointer (&parser));
}

static inline DexFuture *
gitlab_json_parse_bytes (GBytes *bytes)
{
  g_return_val_if_fail (bytes != NULL, NULL);

  return dex_scheduler_spawn (dex_thread_pool_scheduler_get_default (), 0,
                              gitlab_json_parse_bytes_fiber,
                              g_bytes_ref (bytes),
                              (GDestroyNotify)g_bytes_unref);
}

G_END_DECLS
