/* gitlab-utils.c
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include <string.h>

#include "gitlab-utils-private.h"

char **
_gitlab_params_set (char       **params,
                    const char  *key,
                    const char  *value)
{
  guint i;
  gsize keylen;

  g_assert (key != NULL);

  if (value == NULL)
    value = "";

  if (params == NULL)
    params = g_new0 (char *, 1);

  keylen = strlen (key);

  for (i = 0; params[i]; i++)
    {
      if (g_str_has_prefix (params[i], key) && params[i][keylen] == '=')
        {
          g_free (params[i]);
          params[i] = g_strdup_printf ("%s=%s", key, value);
          return params;
        }
    }

  params = g_renew (char *, params, i+2);
  params[i++] = g_strdup_printf ("%s=%s", key, value);
  params[i++] = NULL;

  return params;
}

char **
_gitlab_params_set_int (char       **params,
                        const char  *key,
                        gint64       value)
{
  g_autofree char *str = g_strdup_printf ("%"G_GINT64_FORMAT, value);
  return _gitlab_params_set (params, key, str);
}
