/* gitlab-project-filter.c
 *
 * Copyright 2024 Christian Hergert <chergert@redhat.com>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "gitlab-filter-private.h"
#include "gitlab-project-filter.h"
#include "gitlab-utils-private.h"

struct _GitlabProjectFilter
{
  GObject parent_instance;
  gboolean owned;
};

struct _GitlabProjectFilterClass
{
  GitlabFilterClass parent_class;
};

enum {
  PROP_0,
  PROP_OWNED,
  N_PROPS
};

G_DEFINE_FINAL_TYPE (GitlabProjectFilter, gitlab_project_filter, GITLAB_TYPE_FILTER)

static GParamSpec *properties[N_PROPS];

static char **
gitlab_project_filter_to_params (GitlabFilter *filter)
{
  GitlabProjectFilter *self = (GitlabProjectFilter *)filter;
  char **params = NULL;

  g_assert (GITLAB_IS_PROJECT_FILTER (self));

  if (self->owned)
    params = _gitlab_params_set (params, "owned", "1");

  return params;
}

static void
gitlab_project_filter_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  GitlabProjectFilter *self = GITLAB_PROJECT_FILTER (object);

  switch (prop_id)
    {
    case PROP_OWNED:
      g_value_set_boolean (value, gitlab_project_filter_get_owned (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_project_filter_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  GitlabProjectFilter *self = GITLAB_PROJECT_FILTER (object);

  switch (prop_id)
    {
    case PROP_OWNED:
      gitlab_project_filter_set_owned (self, g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gitlab_project_filter_class_init (GitlabProjectFilterClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GitlabFilterClass *filter_class = GITLAB_FILTER_CLASS (klass);

  object_class->get_property = gitlab_project_filter_get_property;
  object_class->set_property = gitlab_project_filter_set_property;

  filter_class->to_params = gitlab_project_filter_to_params;

  properties[PROP_OWNED] =
    g_param_spec_boolean ("owned", NULL, NULL,
                          FALSE,
                          (G_PARAM_READWRITE |
                           G_PARAM_EXPLICIT_NOTIFY |
                           G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
gitlab_project_filter_init (GitlabProjectFilter *self)
{
}

GitlabProjectFilter *
gitlab_project_filter_new (void)
{
  return g_object_new (GITLAB_TYPE_PROJECT_FILTER, NULL);
}

gboolean
gitlab_project_filter_get_owned (GitlabProjectFilter *self)
{
  g_return_val_if_fail (GITLAB_IS_PROJECT_FILTER (self), FALSE);

  return self->owned;
}

void
gitlab_project_filter_set_owned (GitlabProjectFilter *self,
                                 gboolean             owned)
{
  g_return_if_fail (GITLAB_IS_PROJECT_FILTER (self));

  if (_g_set_boolean (&self->owned, owned))
    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_OWNED]);
}
