Title: Overview

# Overview

Gitlab-GLib is a [GNOME](https://www.gnome.org/) library that provides access
to a gitlab instance through a programmable API.

##  pkg-config name

To build a program that uses Gitlab-GLib, you can use the following command to
get the cflags and libraries necessary to compile and link.

```sh
gcc hello.c `pkg-config --cflags --libs libgitlab-glib-1` -o hello
```
