# Gitlab-GLib

A simple API client for Gitlab.

This came out of a Red Hat Day of Learning where I wanted to learn how the Gitlab API worked.

In particular it explores a way to merge pagination with GListModel.
This is done through the combination of lazy indirection objects (`GitlabListItem`) and dynamically chosen sub-`GListModel`.
When the size of the remote resource is known, a sized list model is used (`GitlabOffsetListModel`).
Otherwise, a dynamically sized list model is used (`GitlabProgressiveListModel`).

## Documentation

The [API Reference](https://chergert.pages.gitlab.gnome.org/gitlab-glib/gitlab-glib-1/index.html) is generated on each commit.

